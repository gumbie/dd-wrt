//////////////////////////////////////////////////////////////////////////////////////////////
//		English reference translation file - DD-WRT V24 SP2       24/JUL/2013				//
//////////////////////////////////////////////////////////////////////////////////////////////

// ** COMMON SHARE LABEL **//
lang_charset.set="UTF-8";

share.annex="Annex Type";
share.apn="APN";
share.pin="PIN";
share.dial="Dial String";
share.mode_3g="접속 타입";
share.mode_3g_auto="Auto 4G/3G/2G";
share.mode_3g_4g="Force LTE/4G";
share.mode_3g_3g="Force 3G";
share.mode_3g_2g="Force 2G";
share.mode_3g_prefer_3g="Prefer 3G";
share.mode_3g_prefer_2g="Prefer 2G";
share.mode_3g_3g2g="3G first, on error 2G";
share.firmware="펌웨어";
share.time="시간";
share.interipaddr="WAN IP 어드레스";
share.more="더 상세히...";
share.help="도움말";
share.enable="유효";
share.enabled="유효";
share.disable="무효";
share.disabled="무효";
share.usrname="사용자 이름";
share.passwd="사용자 암호";
share.hostname="호스트 네임";
share.advanced="고급 설정";
share.vdsl="Advanced VLAN Tagging";
share.vdslvlan7="T-Home VLAN 7 지원";
share.vdslvlan8="T-Home VLAN 8 지원";
share.wan_vlantag="VLAN Tag ID";
share.compression="PPP 압축 (MPPC)";
share.mlppp="Single Line Multi Link";
share.vpi_vci="VPI/VCI";
share.encaps="Encapsulation";
share.payload="Payload Type";
share.domainname="도메인 네임";
share.wandomainname="WAN 도메인 네임";
share.landomainname="LAN 도메인 네임";
share.statu="상태";
share.start="시작";
share.end="끝";
share.proto="프로토콜";
share.ip="IP 어드레스";
share.localip="로컬 IP";
share.remoteip="원격 IP";
share.mac="MAC 어드레스";
share.none="없음";
share.none2="no";
share.both="양방";
share.add="첨가";
share.del="삭제";
share.remove="제거";
share.descr="상세설명";
share.from="From";
share.to="To";
share.about="About";
share.everyday="매일";
share.sun="일요일";
share.sun_s="일";
share.sun_s1="일";
share.mon="월요일";
share.mon_s="월";
share.mon_s1="월";
share.tue="화요일";
share.tue_s="화";
share.tue_s1="화";
share.wed="수요일";
share.wed_s="수";
share.wed_s1="수";
share.thu="목요일";
share.thu_s="목";
share.thu_s1="목";
share.fri="금요일";
share.fri_s="금";
share.fri_s1="금";
share.sat="토요일";
share.sat_s="토";
share.sat_s1="토";
share.jan="1월";
share.feb="2월";
share.mar="3월";
share.apr="4월";
share.may="5월";
share.jun="6월";
share.jul="7월";
share.aug="8월";
share.sep="9월";
share.oct="10월";
share.nov="11월";
share.dec="12월";
share.expires="유효기간";
share.yes="예";
share.no="아니오";
share.filter="필터";
share.deny="거부";
share.range="범위";
share.use="Use";
share.mins="분";
share.secs="초";
share.routername="공유기 이름";
share.manual="수동(메뉴얼)";
share.port="포트";
share.ssid="SSID";
share.channel="채널";
share.frequency="주파수";
share.rssi="Rssi";
share.signal="신호";
share.noise="노이즈";
share.beacon="beacon";
share.openn="열기";
share.dtim="dtim";
share.rates="Rate";
share.rate="Rate";
share.txrate="출력율";
share.rxrate="수신율";
share.low="낮음";
share.medium="보통";
share.high="높음";
share.option="옵션";
share.rule="Rule(규칙)";
share.lan="LAN";
share.point2point="Point to Point";
share.nat="NAT";
share.subnet="Subnet Mask(서브네 마스크)";
share.unmask="표시하기";
share.deflt="초기설정";  //don't use share.default !!!"
share.all="모두";
share.auto="자동";
share.right="오른쪽";
share.left="왼쪽";
share.share_key="공유키";
share.inter="간격 (초)";
share.srv="서비스 이름(서비스명)";
share.port_range="Port 범위";
share.priority="우선순위";
share.gateway="게이트웨이";
share.intrface="인터페이스";  //share.interface은 를 사용하지 마십시요, Mozilla와 호환성의 문제가 있습니다!!!"
share.ccq="CCQ";
share.pintrface="물리적 인터페이스";
share.vintrface="가상 인터페이스";
share.router="공유기";
share.static_lease="Static 할당";
share.srvip="서버 IP";
share.srvipname="서버 IP/Name";
share.localdns="로컬 DNS";
share.minutes="분";
share.oui="OUI 검색";
share.sttic="고정";
share.connecting="접속을 시도하고 있습니다";
share.connect="접속";
share.connected="접속되었습니다";
share.disconnect="접속중지";
share.disconnected="연결이 끊어졌습니다";
share.info="정보";
share.state="상태";
share.mode="모드";
share.encrypt="암호화";
share.key="Key";
share.wireless="무선";
share.dhcp="DHCP";
share.styl="Style";
share.err="에러";
share.errs="에러";
share.meters="미터(m)";
share.ht40="Wide HT40 (20+20 MHz)";
share.ht20="Full HT20 (20 MHz)";
share.dynamicturbo="Dynamic (20/40 MHz)";
share.turbo="Turbo (40 MHz)";
share.full="Full (20 MHz)";
share.half="Half (10 MHz)";
share.quarter="Quarter (5 MHz)";
share.subquarter="Eighth (2.5 MHz)";
share.seealso="다음을 참조하십시요";
share.never="never";
share.unknown="알수없음";
share.expired="기간 만료되었습니다";
share.logout="logout";
share.nmounted="장착되지 않았습니다";
share.fssize="Total / Free Size";
share.src="송신측 Address";
share.dst="수신측 Address";
share.name_resolution="Name Resolution";
share.timeout="Timeout (s)";
share.detail="클릭하시면 자세한 내용을 보실수 있습니다";
share.tmpmem="임시 저장 메모리";
share._1h="매 1시간마다";
share._2h="매 2시간마다";
share._3h="매 3시간마다";
share._4h="매 4시간마다";
share._5h="매 5시간마다";
share._6h="매 6시간마다";
share._12h="매 12시간마다";
share._24h="매일";
share._48h="이틀마다";
share._168h="매주";
share.days="일자";
share.from2=share.from;
share.to2=share.to;
share.days_genetive=share.days;
share.standard="표준";
share.execscript="실행 스크립트";
share.user="사용자";
share.privatekey="사설키(Private Key)";
share.bytes="bytes";
share.kbytes="KB";
share.mbytes="MB";
share.gbytes="GB";
share.preempt="Preemption";
share.acktiming="ACK Timing";
share.broadcast="Broadcast support";
share.secondcharacter="s";
share.change="사용자 암호 변경";

sbutton.save="저장";
sbutton.savetitle="설정을 저정합니다(동작에는 반영하지 않음)";
sbutton.apply="설정 적용";
sbutton.applytitle="설정 즉시 적용";
sbutton.saving="저장되었습니다";
sbutton.cmd="실행중입니다";
sbutton.cancel="설정변경 취소";
sbutton.canceltitle="현재 페이지의 설정변경 취소";
sbutton.refres="표시를 갱신합니다";
sbutton.clos="닫기";
sbutton.del="삭제";
sbutton.continu="계속";
sbutton.add="추가";
sbutton.remove="삭제";
sbutton.modify="변경";
sbutton.deleted="삭제";
sbutton.delall="전체 삭제";
sbutton.autorefresh="표시내용이 자동으로 갱신이 됩니다";
sbutton.backup="설정보존";
sbutton.restore="설정복원";
sbutton.cptotext="편집";
sbutton.runcmd="명령 실행";
sbutton.startup="기동시 스크립트로 저장합니다";
sbutton.shutdown="셧다운 저장";
sbutton.firewall="방화벽 스크립트로 저장합니다";
sbutton.custom="수동 스크립트로 저장합니다";
sbutton.wol="장치 기동";
sbutton.add_wol="Host 추가";
sbutton.manual_wol="수동 기동";
sbutton.summary="개요/요약";
sbutton.filterIP="클라이언트 리스트 편집";
sbutton.filterMac="MAC 필터 리스트 편집";
sbutton.filterSer="서비스 추가/편집";
sbutton.reboot="공유기 리부팅";
sbutton.help="   도움말  ";
sbutton.wl_client_mac="무선 클라이언트 MAC 리스트";
sbutton.update_filter="필터리스터 업데이트";
sbutton.join="참가";
sbutton.log_in="Log 입력";
sbutton.log_out="Log 입력";
sbutton.edit_srv="서비스 추가/편집";
sbutton.routingtab="Routing Table 표시";
sbutton.wanmac="현재 설정중인 PC의 MAC 어드레스 취득";
sbutton.dhcprel="DHCP 할당";
sbutton.dhcpren="DHCP 갱신";
sbutton.survey="사이트 검색";
sbutton.upgrading="업그레이드중";
sbutton.upgrade="업그레이드실시";
sbutton.preview="미리보기";
sbutton.allways_on="항상 On";
sbutton.allways_off="항상 Off";
sbutton.download="다운로드";
sbutton.next="다음으로 &raquo;";
sbutton.prev="&laquo; 되돌아가기";


// ** COMMON ERROR MESSAGES  **//
errmsg.err0="사용자 이름을 반드시 입력하여 주십시요.";
errmsg.err1="공유기 이릉을 반드시 입력하여 주십시요.";
errmsg.err2="입력하신 값이 사용가능한 범위를 초과 하였습니다, 시작 IP 어드레스 혹은 어드레스 범위를 변경하여 주십시요.";
errmsg.err3="적어도 하루이상의 유효기간을 지정하여 주십시요.";
errmsg.err4="종료시간은 시작 시간보다 빨라서는 안됩니다.";
errmsg.err5="MAC 어드레스의 길이가 맞지 않습니다.";
errmsg.err6="사용자 암호를 입력하여 주십시요.";
errmsg.err7="호스트 네임을 입력하여 주십시요.";
errmsg.err8="IP 어드레스 혹은 도메인 네임을 입력하여 주십시요.";
errmsg.err9="사용할 수 없는 DMZ IP 어드레스 입니다.";
errmsg.err10="입력한 사용자 암호가 설정된 암호와 일치하지 않습니다. 사용자 암호를 다시 입력하여 주십시요.";
errmsg.err11="사용자 암호에는 공백이 있었는 안됩니다";
errmsg.err12="실행을 위해서 command(명령)을 입력하여 주십시요.";
errmsg.err13="업그레이드를 실패하였습니다.";
errmsg.err45="HTTPS접속을 지원하지 않습니다! HTTP 모드로 접속하여 주십시요.";
errmsg.err46="HTTPS접속을 지원하지 않습니다";

//common.js error messages
errmsg.err14=" 입력한 값이 범위를 넘었습니다[";
errmsg.err15="WAN MAC 어드레스가 범위를 벋어났습니다[00 - ff].";
errmsg.err16="MAC의 두번째 문자는 다음의[0, 2, 4, 6, 8, A, C, E] 기호에서 선택해야 합니다.";
errmsg.err17="MAC 어드레스가 틀렸습니다.";
errmsg.err18="MAC 어드레스의 길이가 맞지 않습니다.";
errmsg.err19="broadcast MAC 어드레스를 사용하실 수 없습니다.";
errmsg.err20="xx:xx:xx:xx:xx:xx 의 형식으로 MAC 어드레스를 입력하여 주십시요.";
errmsg.err21="유효하지 않은 MAC 어드레스 형식입니다.";
errmsg.err22="WAN MAC 어드레스가 틀렸습니다.";
errmsg.err23="유효하지 않은 16진수 값입니다";
errmsg.err24=" found in MAC address ";
errmsg.err25="키 값이 틀렸습니다.";
errmsg.err26="키의 입력 길이가 틀렸습니다.";
errmsg.err27="허용되지 않은 서브넷 마스크 입니다.";
errmsg.err28=" 허용되지 않은 문자가 포함되어 있습니다, 반드시 [ 0 - 9 ]의 숫자를 사용하여 주십시요.";
errmsg.err29=" 허용되지 않은 ascii 코드가 포함되어 있습니다.";
errmsg.err30=" 허용되지 않은 16진수 숫자가 포함되어 있습니다.";
errmsg.err31=" 허용되지 않은 값입니다.";
errmsg.err32="IP 어드레스와 게이트웨이가 같은 서브넷 마스크에 있지 않습니다.";
errmsg.err33="IP 어드레스와 게이트웨이가 같아서는 안됩니다.";
errmsg.err34=" 은 공백을 포함해서는 안됩니다.";
errmsg.err110="종료넘버는 시작 넘버보다 반드시 커야 합니다";
errmsg.err111="유효하지 않은 IP 어드레스입니다";
errmsg.err112="유효하지 않은 입력 문자입니다 \"<invchars>\" in field \"<필드네임>\"";

//Wol.asp error messages
errmsg.err35="실행을 위해서 반드시 MAC 어드레스를 입력하여 주십시요.";
errmsg.err36="실행을 위해서 네트워크 브로드케스트 어드레스를 입력하여 주십시요.";
errmsg.err37="실행을 위해서 반드시 UDP 포트를 입력하여 주십시요.";

//WL_WPATable.asp error messages
//WPA.asp error messages
errmsg.err38="공유키를 입력하여 주십시요!";
errmsg.err39="유효하지 않은 키입니다, 반드시 8에서 63자리 사이의 ASCII 문자 혹은 64자리 16진수 숫자를 입력하여 주십시요";
errmsg.err40="키를 위해서 키값을 입력하여 주십시요";
errmsg.err41="유효하지 않은 키값의 길이입니다 ";
errmsg.err43="GTK 갱신 간격";

//config.asp error messages
errmsg.err42="복원하실 설정파일이 선택되지 않았습니다.";

//WL_ActiveTable.asp error messages
errmsg.err44="한번에 128대 이상의 클라이언트를 지정하실 수 없습니다.";

//Site_Survey.asp error messages
errmsg.err47="유효하지 않은 SSID 입니다.";

//Wireless_WDS.asp error messages
errmsg.err48="WDS가 현재의 공유기 설정에서 호환되지 않습니다. 다음의 포인트를 확인하여 주시길 바랍니다 :\n * 무선모드가 AP로 설정되어야 합니다 \n * WPA2 는WDS 환경에서 지원하지 않습니다 \n * Wireless Network B-Only 모드의 무선 네트워크는 WDS환경에서 지원하지 않습니다";

//Wireless_radauth.asp error messages
errmsg.err49="Radius 는 AP 모드에서 사용 가능합니다.";

//Wireless_Basic.asp error messages
errmsg.err50="SSID를 입력하여 주십시요.";

// Management.asp error messages
errmsg.err51="공유기가 공장 출하상태의 암호로 설정되어 있습니다, 보안을 위하여 원격 관리 기능을 활성화 하기전에 반드시 사용자 암호를 변경하여 주십시요. OK 버튼을 선택하셔서 암호를 변경하여 주십시요. 취소(Cancel)버튼을 선택하시면 원격 관리 기능이 비활성화되며 메뉴에서 빠져 나옵니다.";
errmsg.err52="확인된 암호가 입력한 암호와 일치하지 않습니다.";

// Port_Services.asp error messages
errmsg.err53="모든 설정을 마치신후, 적용 버튼을 눌러 설정을 저장하여 주십시요.";
errmsg.err54="서비스 네임을 입력하여 주십시요.";
errmsg.err55="서버 네임이 이미 존재합니다.";

// QoS.asp error messages
errmsg.err56="Port 값이 허용된 범위를 넘었습니다 [0 - 65535]의 범위내에 설정하여 주십시요";

// Routing.asp error messages
errmsg.err57="이 항목을 삭제하여도 됩니까?";
errmsg.err103=" 는 너무 작습니다. 최소값이 다음과 같습니다.";

// Status_Lan.asp error messages
errmsg.err58="Lease를 삭제합니다";
errmsg.err581="클릭하시면 pptp 클라이언트와의 접속이 끊어집니다";

//Status_Wireless.asp error messages
errmsg.err59="사용이 불가능합니다! 무선 네트워크를 켜주십시요.";

//Upgrade.asp error messages
errmsg.err60="파일을 선택하여 업그레이드를 진행하십시요.";
errmsg.err61="사용할 수 없는 이미지 파일입니다.";

//Services.asp error messages
errmsg.err62=" 은 이미 정적 Lease 어드레스로 등록되어 있습니다.";

//Saving message
errmsg.err100="처리중입니다...<br/>잠시만 기다려 주십시요.";
errmsg.err101="설정을 복원하고 있습니다...<br/>잠시만 기다려 주십시요.";
errmsg.err102="펌웨어를 업그레이드하고 있습니다...<br/>잠시만 기다려 주십시요";

// **  COMMON MENU ENTRIES  **//
bmenu.setup="설정메뉴";
bmenu.setupbasic="기본 설정";
bmenu.setupddns="DDNS";
bmenu.setupmacclone="MAC 어드레스";
bmenu.setuprouting="경로 설정";
bmenu.setupvlan="VLANs";
bmenu.setupeop="EoIP 터널(Tunnel)";
bmenu.networking="네트워킹";

bmenu.wireless="무선설정";
bmenu.wirelessBasic="기본 설정";
bmenu.wirelessRadius="Radius";
bmenu.wirelessSuperchannel="SuperChannel";
bmenu.wimax="WiMAX";
bmenu.wirelessSecurity="무선 보안";
bmenu.wirelessAoss="AOSS";
bmenu.wirelessAossWPS="AOSS / WPS";
bmenu.wirelessMac="MAC 필터";
bmenu.wirelessAdvanced="고급 설정";
bmenu.wirelessAdvancedwl0="WL0-Advanced";
bmenu.wirelessAdvancedwl1="WL1-Advanced";
bmenu.wirelessWds="WDS";
bmenu.wirelessWds0="Ath0-WDS";
bmenu.wirelessWds1="Ath1-WDS";
bmenu.wirelessWds2="Ath2-WDS";
bmenu.wirelessWds3="Ath3-WDS";
bmenu.wirelessWdswl0="WL0-WDS";
bmenu.wirelessWdswl1="WL1-WDS";

bmenu.security="보안설정";
bmenu.firwall="방화벽(Firewall)";
bmenu.vpn="VPN 통과(Passthrough)";

bmenu.accrestriction="접근 제한";
bmenu.webaccess="인터넷(WAN) 억세스";

bmenu.applications="NAT / QoS";
bmenu.applicationsprforwarding="어드레스 변환(Port범위)";
bmenu.applicationspforwarding="어드레스 변환(Port)";
bmenu.applicationsptriggering="어드레스 변환(Trigger 지정)";
bmenu.applicationsUpnp="UPnP";
bmenu.applicationsDMZ="DMZ";
bmenu.applicationsQoS="QoS";
bmenu.applicationsP2P="P2P";

bmenu.sipath="SIPatH";
bmenu.sipathoverview="개요";
bmenu.sipathphone="연락처";
bmenu.sipathstatus="상태";

bmenu.admin="관리자 메뉴";
bmenu.adminManagement="관리메뉴";
bmenu.adminAlive="켜진 상태 유지";
bmenu.adminLog="Log";
bmenu.adminDiag="Commands";
bmenu.adminWol="WOL";
bmenu.adminFactory="공장 초기 설정";
bmenu.adminUpgrade="펌웨어 업그레이드";
bmenu.adminBackup="백업";

bmenu.services="서비스";
bmenu.servicesServices="서비스 메뉴";
bmenu.servicesRadius="FreeRadius";
bmenu.servicesPppoesrv="PPPoE 서버";
bmenu.servicesPptp="VPN";
bmenu.servicesUSB="USB";
bmenu.servicesNAS="NAS";
bmenu.servicesHotspot="핫스팟(Hotspot)";
bmenu.servicesNintendo="닌텐도";
bmenu.servicesMilkfish="SIP Proxy";
//bmenu.servicesAnchorFree="My Ad Network";

bmenu.statu="공유기상태";
bmenu.statuRouter="공유기정보";
bmenu.statuInet="WAN";
bmenu.statuLAN="LAN";
bmenu.statuSputnik="Sputnik Agent";
bmenu.statuWLAN="무선 LAN";
bmenu.statuVPN="OpenVPN";
bmenu.statuBand="대역폭";
bmenu.statuSysInfo="시스템 정보";
bmenu.statuActivate="Activate";
bmenu.statuMyPage="My Page";
bmenu.statuGpio="GPIO I/O";

bmenu.setupnetw="네트워크";
bmenu.adminman="관리메뉴";

// ** Alive.asp **//
alive.titl="켜진 상태유지";
alive.h2="켜진 상태유지";
alive.legend="일정주기로 리부팅하기";
alive.sevr1="일정주기로 리부팅하기";
alive.hour="지정 시각에 재기동 실시";
alive.legend2="WDS/접속 감시";
alive.sevr2="WDS의 접속상대의 정기적으로 접속확인";
alive.IP="IP Addresses";
alive.legend3="Proxy/접속 감시";
alive.sevr3="Proxy 서버의 정기적 접속 확인";
alive.IP2="Proxy IP Address";
alive.port="Proxy Port";

//help container
halive.right2="공유기 재기동을 실행하는 시각을 지정합니다. 시각 지정을 위해서, Cron 서비스가 활성화되어야 합니다.";
halive.right4="IP 어드레스는 최대 3개까지 지정할 수 있습니다. 복수의 IP를 지정하려면 <em>SPACE</em> 로 구분하여 주십시요.<br/>IP 형식은 다음과 같습니다 xxx.xxx.xxx.xxx .";

// ** config.asp **//
config.titl="설정보존 & 복원";
config.h2="설정 파라미터 보존";
config.legend="설정 보존";
config.mess1="Click the \"" + sbutton.backup + "\" 버튼을 클릭하시면 설정을 보존한 파일이 컴퓨터에 다운로드됩니다.";
config.h22="설정 파라미터 복원";
config.legend2="설정 복원";
config.mess2="복원하실 설정파일을 선택하여 주십시요";
config.mess3=" !!! 경  고 !!! ";
config.mess4="같은 펌웨어와 같은 모델의 공유기에서 백업된 파일만을 업로드하여 주십시요.<br />현재와 다른 인터페이스에서 작성된 파일을 업로드하시면 안됩니다!";

//help container
hconfig.right2="리셋이 필요한 경우 혹은 공장 초기 설정으로 되돌려야 할 경우를 대비하여, 현재의 환경 설정을 백업하여 주십시요.<br /><br /> <em>Backup</em> 버튼을 클릭하시면 현재 설정의 백업을 진행합니다"
hconfig.right4="<em>Browse...</em> 버튼을 클릭하여 PC에 저정된 환경 설정 파일을 검색하여 주십시요.<br /><br /><em>" + sbutton.restore + "</em> 을 클릭하시면 현재의 모든 설정이 선택하신 파일로 덮어쓰기를 합니다.";

// ** DDNS.asp **//
ddns.titl="Dynamic DNS";
ddns.h2="유동 도메인 네임 서버 시스템 (DDNS)";
ddns.legend="DDNS";
ddns.srv="DDNS 서비스";
ddns.emailaddr="E-mail 주소";
ddns.typ="타입";
ddns.dynamic="Dynamic";
ddns.custom="Custom";
ddns.wildcard="Wildcard";
ddns.statu="DDNS 상태";
ddns.system="DYNDNS 서버";
ddns.options="추가 DDNS 옵션";
ddns.forceupd="강제 업데이트 간격";
ddns.wanip="외부 ip check를 사용하지 마십시요";

ddnsm.all_closed="DDNS 서버와 연결이 끊어져 있습니다";
ddnsm.all_resolving="도메인 네임을 확인하고 있습니다";
ddnsm.all_errresolv="도메인 네임 확인을 실패하였습니다";
ddnsm.all_connecting="DNS서버에 접속중입니다";
ddnsm.all_connectfail="DNS서버와의 접속에 실패했습니다";
ddnsm.all_disabled="DDNS 기능이 꺼져있습니다";
ddnsm.all_noip="인터넷(WAN)에 연결되어 있지 않습니다";

//help container
hddns.right2="DDNS는 외부에서 장비 (또는 내부 네트워크) 접근을 IP 주소 대신 도메인 이름으로 통신 할 수 있는 기능입니다. 기기가 IP 주소를 검색하면 DDNS 서비스를 제공하는 서버에 정보를 업데이트하며 DDNS 서비스는 항상 최신의 IP 주소를 다른 호스트에 알려줍니다. DynDNS.org, freedns.afraid.org, ZoneEdit.com, No-IP.com 등에 등록하시면 DDNS 기능을 사용할 수 있게됩니다.";
hddns.right4="IP 주소 정보를 업데이트하는 간격을 지정할 수 있습니다. IP 주소가 변경된 경우에는이 설정에 관계없이 자동으로 업데이트가 이루어집니다. DDNS 서비스 환경이 불안정한 경우 등에 사용하십시요";

// ** Diagnostics.asp **//
diag.titl="Diagnostics(진단명령실행)";
diag.h2="Diagnostics(진단명령실행)";
diag.legend="Command Shell";
diag.cmd="실행 명령(Command)";
diag.startup="기동시 스크립트";
diag.shutdown="Shutdown";
diag.firewall="방화벽 스크립트";
diag.custom="수동 스크립트";

//help container
hdiag.right2="Web 인터페이스에서 콘솔 명령을 실행하는 것이 가능합니다. Text area에 실행하고 싶은 명령을 입력하신후<em>" + sbutton.runcmd + "</ em> 을 클릭하여 실행할 수 있습니다. 콘솔에서 실행 결과가 하단에 표시됩니다.";


// ** DMZ.asp **//
dmz.titl="DMZ";
dmz.h2="Demilitarized Zone (DMZ설정)";
dmz.legend="DMZ";
dmz.serv="DMZ 사용(설정)";
dmz.host="DMZ Host IP Address";

//help container
hdmz.right2="이 기능을 사용하면 LAN 측 네트워크의 특정 호스트를 외부에 공개 할 수 있습니다. 활성화하면 지정된 호스트의 모든 Port가 외부에 공개됩니다.";

// ** Factory_Defaults.asp **//
factdef.titl="설정 초기화";
factdef.h2="설정 초기화";
factdef.legend="공유기 설정 리셋";
factdef.restore="공장 출하상태로 복구";
factdef.mess1="경고! 만일 OK를 클릭하시면 장치의 모든 설정이 공장 출하시의 초기설정으로 리셋이 되면 현재의 모든 설정이 삭제됩니다.";

//help container
hfactdef.right1="모든 설정을 리셋한 후 공장 출하시의 초기설정으로 되돌아가며, 현재의 모든 설정은 삭제됩니다.";

// ** FilterIPMAC.asp **//
filterIP.titl="클라이언트 리스트";
filterIP.h2="클라이언트 리스트";
filterIP.h3="클라이언트의 MAC어드레스를 기입하여 주십시요:다음과 같은 형식으로 입력하여 주십시요 (xx:xx:xx:xx:xx:xx)";
filterIP.h32="클라이언트의 IP어드레스를 기입하여 주십시요";
filterIP.h33="클라이언트의 IP어드레스의 범위를 기입하여 주십시요";
filterIP.ip_range="IP 어드레스 범위";

// ** Filter.asp **//
filter.titl="접근 제한(타이머)";
filter.h2="인터넷 억세스 설정";
filter.legend="억세스 사용자 정의";
filter.pol="사용자 정의";
filter.polname="사용자 정의 이름";
filter.pcs="PCs(단말 지정)";
filter.polallow="지정한 날과 시간에 한해서 인터넷 접속허가.";
filter.legend2="날짜";
filter.time="시간";
filter.h24="24 시간(종일)";
filter.legend3="서비스 차단";
filter.catchall="모든 P2P 프로토콜을 차단";
filter.legend4="특정 웹사이트 차단(URL 지정)";
filter.legend5="특정 키워드로 관련 웹사이트 차단";
filter.mess1="사용자정의(Policy) 삭제?";
filter.mess2="최소한 하루 이상의 요일/일자를 지정하여 주십시요.";
filter.mess3="종료 시간은 시작 시간보다 빠르게 설정 할 수 없습니다.";

//help container
hfilter.right2="최대 10 개의 사용자 정의를 설정할 수 있습니다. <em>"+ sbutton.del + "</ em>을 클릭하면 선택한 사용자 정의가 삭제됩니다. 또한 <em>"+ sbutton.summary + "</ em>을 클릭하면 현재 설정된 사용자 정의 목록이 표시됩니다.";
hfilter.right4="사용자 정의를 활성화 혹은 비활성화 하실 수 있습니다.";
hfilter.right6="사용자 정의에 임의의 이름을 부여할 수 있습니다.";
hfilter.right8="사용자 정의를 실행할 요일을 지정하실 수 있습니다.";
hfilter.right10="사용자 정의를 실행할 시간을 지정하실 수 있습니다.";
hfilter.right12="통신을 차단하는 서비스를 지정할 수 있습니다. <em>"+ sbutton.filterSer + "</ em>을 클릭하여 서비스의 설정을 수정 할 수 있습니다.";
hfilter.right14="URL을 작성하셔서 접근을 제한하실 웹사이트를 지정하실 수 있습니다.";
hfilter.right16="키워드를 지정하셔서 특정 키워드를 포함하고 있는 웹사이트의 접근을 제한하실 수 있습니다.";

// ** FilterSummary.asp **//
filterSum.titl="접근 제한 일람";
filterSum.h2="인터넷 사용자 정의 일람";
filterSum.polnum="번호(No.)";
filterSum.polday="시각";

// ** Firewall.asp **//
firewall.titl="Firewall";
firewall.h2="보안 설정";
firewall.legend="방화벽 보호";
firewall.firewall="SPI Firewall";
firewall.legend2="추가 필터";
firewall.proxy="Proxy를 필터링합니다";
firewall.cookies="Cookie를 필터링합니다";
firewall.applet="Java Applet를 필터링합니다";
firewall.activex="ActiveX를 필터링합니다";
firewall.legend3="인터넷접속(WAN)측의 패킷을 끊습니다";
firewall.legend4="WAN DoS를 지연시킵니다/억제시킵니다";
firewall.ping="알수 없는 WAN측의 요구를 막습니다 (ping에 응답하지 않습니다)";
firewall.muticast="멀티케스팅(다중 작업)을 필터링합니다";
firewall.ssh="SSH 접근을 제한합니다";
firewall.telnet="Telnet 접근은 제한합니다";
firewall.pptp="PPTP 서버 접근을 제한합니다";
firewall.ftp="FTP 서버 접근을 제한합니다";
filter.nat="WAN NAT Redirection을 필터링합니다";
filter.port113="IDENT (Port 113)를 필터링합니다";
filter.snmp="WAN SNMP 접근을 차단합니다";

//help container
hfirewall.right2="SPI firewall 기능을 활성화 혹은 비활성화의 변경이 가능합니다.";

// ** Forward.asp **//
prforward.titl="Port Range Forwarding";
prforward.h2="Port Range Forwarding";
prforward.legend="Forwards(어드레스교환 법칙)";
prforward.app="어플리케이션";

//help container
hprforward.right2="인터넷을 이용하는 일부 응용 프로그램 (네트워크 게임 등)은 특정 포트를 개방하도록 하는 설정이 필요한 경우가 있습니다. 여기에서는 인터넷 측에서 특정 Port의 통신을 요구하는 경우 지정한 컴퓨터로 경로를 설정하실 수 있습니다. 보안을 위해 여기서 지정하신 포트는 응용 프로그램을 동작시키기 위한 최소한으로 제한하시고, 또한 응용 프로그램의 사용이 끝나면, 설정을 무효화 시켜주시기 바랍니다.";

// ** P2P.asp **//
p2p.titl="Peer-to-Peer(P2P어플리케이션)";
p2p.h2="BitTorrent Client";
p2p.legend="CTorrent";
p2p.ctorrent_srv="Ctorrent 서비스";

//help container
//hpp2p.right2="인터넷을 이용하는 일부 응용 프로그램 (네트워크 게임 등)은 특정 포트를 개방하도록 하는 설정이 필요한 경우가 있습니다. 여기에서는 인터넷 측에서 특정 Port의 통신을 요구하는 경우 지정한 컴퓨터로 경로를 설정하실 수 있습니다. 보안을 위해 여기서 지정하신 포트는 응용 프로그램을 동작시키기위한 최소한으로 제한하시고, 또한 응용 프로그램의 사용이 끝나면,  <em>" + share.enable +"</em> 체크박스를 해제하시길 부탁드립니다.";

// ** ForwardSpec.asp **//
pforward.titl="Port Forwarding(포트포워딩)";
pforward.h2="Port Forwarding(포트포워딩)";
pforward.legend="Forwards";
pforward.app="어플리케이션";
pforward.src="Source Net";
pforward.from="Port from(WAN측의 포트)";
pforward.to="Port to(LAN측의 포트)";

//help container
hpforward.right2="인터넷을 이용하는 일부 응용 프로그램 (네트워크 게임 등)은 특정 포트를 개방하도록 하는 설정이 필요한 경우가 있습니다. 여기에서는 인터넷 측에서 특정 Port의 통신을 요구하는 경우 지정한 컴퓨터로 경로를 설정하실 수 있습니다. 보안을 위해 여기서 지정하신 포트는 응용 프로그램을 동작시키기위한 최소한으로 제한하시고, 또한 응용 프로그램의 사용이 끝나면, <em>Enable</em> 체크박스의 선택을 반드시 해제하시길 부탁드립니다.";

// ** USB.asp **//
usb.titl="USB";
usb.usb_legend="USB 서포트";
usb.usb_core="Core USB 서포트";
usb.usb_uhci="USB 1.1 지원 (UHCI)";
usb.usb_ohci="USB 1.1 지원 (OHCI)";
usb.usb_ehci="USB 2.0 지원";
usb.usb_storage="USB 저장장치 지원";
usb.usb_ip="USB Over IP";
usb.usb_printer="USB 프린터 지원";
usb.usb_automnt="드리이브 자동 인식";
usb.usb_mntpoint="Disk Mount Point";
usb.usb_runonmount="Run-on-mount Script Name";
usb.usb_diskinfo="디스크 정보";

// ** NAS.asp **//
nas.titl="NAS";
nas.proftpd_legend="FTP 서버";
nas.proftpd_srv="ProFTPD";
nas.proftpd_port="Server Port";
nas.proftpd_dir="Files Directory(파일디렉토리)";
nas.proftpd_passw="사용자 암호 리스트";
nas.proftpd_writeen="내용기입 허가";
nas.proftpd_anon="Anonymous 익명으로 로그인 (읽기만 가능)";
nas.proftpd_anon_subdir="Anonymous Home Sub-directory";
nas.samba3_legend="파일 공유";
nas.samba3="Samba Server";

hnas.right2="사용자 암호 목록 : 한 줄에 하나의 사용자 암호를 입력합니다. 암호는 일반 텍스트 또는 MD5 암호로 설정할 수 있습니다.";

// ** DLNA **
nas.dlna_legend="DLNA Server";
nas.dlna_srv="MiniDLNA";
nas.dlna_thumb="Cover 아트워크(Artwork) 포함";
nas.dlna_dir="파일 디렉토리";

// ** Hotspot.asp **//
hotspot.titl="Hotspot";
hotspot.h2="Hotspot 서비스 설정";
hotspot.legend="Chillispot";
hotspot.nowifibridge="LAN측 브릿지에서 Wifi(무선랜)측 장치를 분리";
hotspot.hotspot="Chillispot(칠리스팟)";
hotspot.pserver="메인 Radius Server IP/DNS";
hotspot.bserver="백업 Radius Server IP/DNS";
hotspot.dns="DNS Server IP";
hotspot.url="URL 경로 재설정";
hotspot.dhcp="DHCP 인터페이스";
hotspot.radnas="Radius NAS ID";
hotspot.net="Hotspot Network";
hotspot.uam="UAM Secret(암호)";
hotspot.uamserver="UAM 서버";
hotspot.uamport="UAM Port";
hotspot.uamdns="UAM Any DNS";
hotspot.allowuam="UAM Allowed (comma seperated)";
hotspot.allowuad="UAM Domains (space separated)";
hotspot.macauth="MAC 어드레스 인증";
hotspot.macpasswd="MAC 어드레스 암호";
hotspot.sec8021Xauth="802.1X 인증 (EAP)";
hotspot.option="Chillispot 추가 옵션";
hotspot.fon_chilli="Chillispot 로컬 사용자 관리";
hotspot.fon_user="사용자 리스트";
hotspot.http_legend="HTTP 경로 재설정";
hotspot.http_srv="HTTP 경로 재설정";
hotspot.http_ip="HTTP 수신자 IP";
hotspot.http_port="HTTP 수신자 Port";
hotspot.http_net="HTTP Source Network";
hotspot.nocat_legend="NoCatSplash";
hotspot.nocat_srv="NoCatSplash";
hotspot.nocat_gateway="Gateway Name";
hotspot.nocat_gatewayaddr="Gateway IP 어드레스";
hotspot.nocat_home="홈페이지";
hotspot.nocat_extifname="외부 인터페이스";
hotspot.nocat_ifname="내부 인터페이스";
hotspot.nocat_redirect="홈페이지 경로 재설정";
hotspot.nocat_allowweb="허가된 Web 호스트";
hotspot.nocat_docroot="Document Root";
hotspot.nocat_splash="Splash URL";
hotspot.nocat_port="거부할 Port";
hotspot.nocat_timeout="Login Timeout";
hotspot.nocat_verbose="더 상세하게";
hotspot.nocat_route="경로 설정만 하기";
hotspot.nocat_MAClist="MAC White List";
hotspot.smtp_legend="SMTP 경로 재설정";
hotspot.smtp_srv="SMTP 경로 재설정";
hotspot.smtp_ip="SMTP 수신자 IP";
hotspot.smtp_net="SMTP Source Network";
hotspot.shat_legend="Zero IP Config(설정)";
hotspot.shat_srv="Zero IP Config(설정)";
hotspot.shat_srv2="Zero IP 설정 유효화";
hotspot.sputnik_legend="Sputnik";
hotspot.sputnik_srv="Sputnik Agent(서비스)";
hotspot.sputnik_mode="Sputnik 모드";
hotspot.sputnik_id="Sputnik 서버 ID";
hotspot.sputnik_instant="Sputnik Instant 설정 사용";
hotspot.sputnik_express="SputnikNet Express 사용";
hotspot.sputnik_about="Sputnik 에 관하여";
hotspot.sputnik_learn="상세한 설정방법은 여기에";
hotspot.wifidog_legend="Wifidog";
hotspot.wifidog_srv="Wifidog Gateway";
hotspot.wifidog_id="Gateway ID";
hotspot.wifidog_url="포탈사이트 URL";
hotspot.wifidog_port="Port(포트)";
hotspot.wifidog_httpdname="Web Server Name";
hotspot.wifidog_httpdconn="최대 사용자수";
hotspot.wifidog_checkinter="체크 간격 (초)";
hotspot.wifidog_checktimeout="Client Timeout";
hotspot.wifidog_tmaclist="등록 MAC 어드레스 리스트";
hotspot.wifidog_authsrv="AuthServer(인증서버) Hostname";
hotspot.wifidog_authsrvssl="AuthServer(인증서버)접속에 SSL을 이용";
hotspot.wifidog_authsrvsslport="AuthServer(인증서버)의 SSL 포트";
hotspot.wifidog_authsrvhttpport="AuthServer(인증서버)의 HTTP 포트";
hotspot.wifidog_authsrvpath="AuthServer Path";
hotspot.wifidog_config="Firewall Ruleset";
hotspot.wifidog_messagefile="HTML Message File for Wifidog";
hotspot.wifidog_realm="HTTP Server Realm";
hotspot.wifidog_username="HTTP 서버 사용자 이름";
hotspot.wifidog_password="HTTP 서버 사용자 암호";
hotspot.wifidog_auth="HTTP 서버 인증 지원";

//help container
hstatus_hots.right1="conup/condown:<br><i>When USB or JFFS is mounted to jffs, connection scripts can be used in /jffs/etc/chilli/</i><br>Local Users:<br><i>When only local users are used set primary radius to 127.0.0.1</i>";

// ** Hotspotsystem **//
hotspotsys.legend="Hotspot System";
hotspotsys.nobridge="LAN Bridge에서 Wifi를 분리시킴";
hotspotsys.uamenable="특수 설정";
hotspotsys.loginonsplash="Splash page에 로그온";
hotspotsys.allowuam="UAM 허가";
hotspotsys.allowuad="UAM 도메인 (space separated)";
hotspotsys.whitelabelproto="White Label 프로토콜";
hotspotsys.whitelabel="White Label 도메인";
hotspotsys.operatorid="운영자 사용자 이름";
hotspotsys.locationid="Location ID";
hotspotsys.dhcp="DHCP 인터페이스";
hotspotsys.net="리모트 네트워크";
hotspotsys.customsplash="Custom Splash Page (Walled Garden)";

//anchorfree.anchorfree="AnchorFree";
//anchorfree.titl="My Ad Network";
//anchorfree.h2="AnchorFree Ad Network";
//anchorfree.anchorfree_revenue="광고를 지원하는 AnchorFree 핫스팟을 구축하여 수입 충당";
//anchorfree.email="수입 내역서를 메일로 보내기";
//anchorfree.ssid="다른 SSID 사용";
//anchorfree.ssid_name="SSID";
//anchorfree.address_1="상세 주소 1";
//anchorfree.address_2="상세 주소 2";
//anchorfree.city="도시";
//anchorfree.zip="우편번호";
//anchorfree.state="광역시/특별시/도";
//anchorfree.country="국가";
//anchorfree.category="카테고리";
//anchorfree.publish="무료 WiFi 핫스팟 맵에 공개";
//anchorfree.serviceid="서비스 ID";
//anchorfree.servicestatus="서비스 상태";
//anchorfree.agreement="이용 약관";
//anchorfree.agree="이용 약관을 이해하며 동의 합니다";
//anchorfree.validaddr="Wifi 핫스팟 맵에 공개되기 위해서는 유효한 어드리스 필드가 제공되어야 합니다!";
//anchorfree.validcity="Wifi 핫스팟 맵에 공개되기 위해서는 유효한 도시 혹은 우편번호가 제공되어야 합니다!";
//anchorfree.validcat="핫스팟에 사용하실 광고 카테고리를 선택하여 주십시요";
//anchorfree.validcountry="핫스팟에 사용하실 국가를 선택하여 주십시요";
//anchorfree.validterms="이용 약관에 반드시 동의 하여야 합니다!";
//
//hanchorfree.right1="AnchorFree의 핫스팟 광고 네트워크에 참가합니다";
//hanchorfree.right2="AnchorFree 은 DD-WRT 의 고객들이 핫스팟 광고 네트워크에 참가할 수 있는 기회를 제공하여 드립니다";
//hanchorfree.right3="Generate incremental revenue with advertising from AnchorFree";
//hanchorfree.right4="By activating this feature and creating an account with AnchorFree (it's free and easy), a persistent advertising frame is inserted directly into users web browsers, which will earn you a payment every month.  Earn a minimum monthly threshold of $25 and AnchorFree will automatically credit your account with funds.";
//hanchorfree.right5="더 자세한 정보를 위해서, 다음의 웹사이트를 방문하여 주십시요 www.anchorfree.com";
//hanchorfree.right6="활성화 시키는 것은 어렵지 안습니다";
//hanchorfree.right7="일단 활성화를 시키면 AnchorFree에서 등록하신 계정으로 (지정할)핫스팟의 최적화에 관한 간단한 설치 안내, FAQ, 그외 관련 정보에 관한 email을 발송합니다. 이 설정화면을 통하여, AnchorFree 에서 이 공유기의 인터넷 회선을 통하여 웹브라우저에 간단한 광고프레임을 직접 삽입합니다.";
//hanchorfree.right8="고객지원";
//hanchorfree.right9="궁금한 점이 있으면 다음의 주소로 문의하시길 바랍니다 boxhelp@anchorfree.com";

// ** Info.htm **//
info.titl="정보";
info.h2="시스템 정보";
info.wlanmac="무선 MAC 어드레스";
info.srv="서비스";
info.ap="Access Point";

// ** index_heartbeat.asp **//
idx_h.srv="Heart Beat Server";
idx_h.con_strgy="접속 확인 방법";
idx_h.max_idle="On Demand 접속: 최대 대기 시간";
idx_h.alive="Keep Alive: Redial Period(간격)";
idx_h.reconnect="강제 재접속";

// ** index_l2tp.asp **//
idx_l.srv="Gateway (L2TP Server)";
idx_l.req_chap="CHAP 요구";
idx_l.ref_pap="PAP 거부";
idx_l.req_auth="인증요구";

// ** index_pppoe.asp **//
idx_pppoe.use_rp="RP PPPoE 사용";

// ** index_pptp.asp **//
idx_pptp.srv="DHCP 사용";
idx_pptp.wan_ip="WAN IP Address";
idx_pptp.gateway="Gateway (PPTP Server)";
//idx_pptp.encrypt="PPTP 암호화"; //should be needed anymore. del soon"
idx_pptp.reorder="패킷 재요구";
idx_pptp.addopt="추가 PPTP 옵션";


// ** index_static.asp **//
idx_static.dns="정적(고정)DNS";


// ** index.asp **//
idx.titl="기본 설정";
idx.h2="WAN(인터넷) 설정";
idx.h22="무선 설정";
idx.legend="WAN(인터넷) 접속 방법";
idx.conn_type="접속 방법";
idx.stp="STP";
idx.stp_mess="(COMCAST ISP를 이용시 사용할 수 없습니다)";
idx.optional="상세 설정";
idx.mtu="MTU";
idx.txqlen="TX Queue Length";
idx.h23="네트워크 설정";
idx.routerip="공유기(LAN측) IP";
idx.lanip="Local IP Address";
idx.legend2="WAN Port";
idx.wantoswitch="스위치에 WAN(인터넷)측 Port 설정";
idx.legend3="시간 설정";
idx.timeset="Time Zone(타임존)";
idx.static_ip="고정 IP(수동 설정)";
idx.dhcp="IP자동 설정 - DHCP";
idx.dhcp_legend="네트워크 어드레스 서버 설정 (DHCP)";
idx.dhcp_type="DHCP Type";
idx.dhcp_srv="DHCP 서버";
idx.dhcp_fwd="DHCP Forwarder";
idx.dhcp_start="할당이 시작되는 IP Address";
idx.dhcp_end="할당이 끝나는 IP Address"; //used in Status_Lan.asp"
idx.dhcp_maxusers="최대 DHCP 사용자수";
idx.dhcp_lease="사용자에 할당된 시간";
idx.dhcp_dnsmasq="DHCP할당에 DNSMasq을 사용";
idx.dns_dnsmasq="DNS 할당에 DNSMasq을 사용";
idx.auth_dnsmasq="DHCP-Authoritative";
idx.summt_opt1="없음";
idx.summt_opt2="4월 첫째 일요일 - 10월 마지막 일요일";
idx.summt_opt3="3월 마지막 일요일 - 10월 마지막 일요일";
idx.summt_opt4="10월 마지막 일요일 - 3월 마지막 일요일";
idx.summt_opt5="3월 2번째 일요일 - 11월 첫번째 일요일";
idx.summt_opt6="10월 첫번째 일요일 - 3월 3번째 일요일";
idx.summt_opt7="9월 마지막 일요일 - 4월 첫번째 일요일";
idx.summt_opt8="10월 3번째 일요일 - 3월 3번째 일요일";
idx.summt_opt9="10월 첫번째 일요일 - 4월 첫번째 일요일";
idx.summt_opt10="10월 3번째 일요일 - 2월 3번째 일요일";
idx.portsetup="Port 설정";
idx.wanport="WAN(인터넷측) Port 설정";
idx.ntp_client="NTP Client";

//help container
hidx.right2="DHCP 서버에서 IP 주소를 자동으로 가져옵니다. 대부분의 케이블 TV 에서 이 연결 방식을 채용하고 있습니다.";
hidx.right4="인터넷 서비스 업체로부터 부여받은 Host name을 입력하여 주십시요";
hidx.right6="인터넷 서비스 업체로부터 부여받은 도메인 네임(Domain Name)을 입력하여 주십시요";
hidx.right8="이 공유기의 LAN측 IP 어드레스입니다.";
hidx.right10="이 공유기의 Subnet mask입니다.";
hidx.right12="이 장비에서 DHCP 서버가 LAN상의 네트워크 장비의 IP 주소를 관리하도록 합니다.";
hidx.right14="다른 장비에 할당하는 IP 어드레스 범위의 시작점을 입력하여 주십시요.";
hidx.right16="할당하는 IP Address 수를 입력합니다. 시작하는 어드레스부터 끝나는 어드레스(시작 어드레스+최대 할당 어드레스 수)는 LAN측 네트워크 어드레스의 범위를 초과하여서는 안됩니다.";
hidx.right18="라우터에서 타임존(UTC)를 설정하거나, 현지 시간을 설정하실 수 있으며, 서머타임(DST)시간도 설정하실 수 있습니다.";
hidx.sas="이 설정 도우미 (Setup assistant guide)는 간단한 설정순서로 공유기 설정을 안내해드립니다.";

// ** DSL ** //
dsl.status="DSL 상태";
dsl.annex=" DSL Annex";
dsl.iface_status="접속 상태";
dsl.datarate="접속 속도 (up/down)";
dsl.snr="DSL Signal(신호) (up/down)";

// ** Join.asp **//

//sshd.webservices
join.titl="참가";
join.mess1="네트워크에 접속이 완료되었습니다: ";


// ** Log_incoming.asp **//
log_in.titl="Incoming Log Table";
log_in.h2="Incoming Log Table";
log_in.th_ip="송신측 IP";
log_in.th_port="수신측 Port 넘버";


// ** Log_outgoing.asp **//
log_out.titl="Outgoing Log Table(송신메세지)";
log_out.h2="Outgoing Log Table(송신메세지)";
log_out.th_lanip="LAN IP";
log_out.th_wanip="Destination URL/IP(수신측 어드레스/호스트명)";
log_out.th_port="Service/Port Number";


// ** Log.asp **//
log.titl="Log";
log.h2="Log Management(방화벽 Log설정)";
log.legend="Log";
log.lvl="Log Level(Log 출력 레벨)";
log.drop="Dropped(Drop한 패킷 기록)";
log.reject="Rejected(거부된 패킷 기록)";
log.accept="Accepted(수신된 패킷 기록)";


// ** Management.asp **//
management.titl="관리 설정";
management.h2="공유기 관리";
management.changepassword="초기 설정된 사용자 이름과 사용자 암호의 조합입니다, 공유기가 위험에 노출될 수 있으니, 다음 순서를 따라 사용자 이름과 암호를 변경하여 주십시요!";
management.psswd_legend="공유기 사용자 암호";
management.psswd_user="공유기 사용자 이름";
management.psswd_pass="공유기 사용자 암호";
management.pass_conf="확인을 위하여 한번더 입력하여 주십시요";
management.remote_legend="Remote Access(원격 접근)";
management.remote_gui="Web GUI Management(HTTP관리 인터페이스)";
management.remote_https="HTTPS 사용";
management.remote_guiport="Web GUI Port";
management.remote_ssh="SSH 관리 인터페이스";
management.remote_sshport="SSH Remote Port";
management.remote_telnet="Telnet 관리";
management.remote_telnetport="Telnet Remote Port";
management.remote_allowanyip="모든 Remote IP를 허가";
management.remote_ip="허가된 Remote IP 범위";
management.web_legend="Web 관리 인터페이스";
management.web_refresh="자동 재표시 간격 (초)";
management.web_sysinfo="시스템 정보 표시";
management.web_sysinfopass="시스템 정보 표시 화면을 인증으로 보호";
management.web_sysinfomasq="시스템 정보에 MAC 어드레스를 감추기";
management.boot_legend="Boot 지연";
management.boot_srv="Boot 지연";
management.cron_legend="Cron";
management.cron_srvd="Cron 서비스";
management.cron_jobs="추가 Cron Jobs";
management.loop_legend="Loopback";
management.loop_srv="Loopback";
management.wifi_legend="802.1x";
management.wifi_srv="802.1x";
management.rst_legend="리셋 버튼";
management.rst_srv="리셋 버튼";
management.routing_legend="Routing(경로화 설정)";
management.routing_srv="Routing(경로화 설정)";
management.ipv6_legend="IPv6 지원";
management.ipv6_srv="IPv6";
management.ipv6_rad="Radvd 유효화";
management.ipv6_radconf="Radvd 설정";
management.jffs_legend="JFFS2 지원";
management.jffs_srv="JFFS2";
management.jffs_clean="JFFS2의 보존 영역을 초기화";
management.lang_legend="Language Selection 언어 선택";
management.lang_srv="Language 언어";
management.lang_bulgarian="Bulgarian 불가리아어";
management.lang_chinese_traditional="Chinese Traditional (대만/홍콩)";
management.lang_chinese_simplified="Chinese Simplified 중국어 간체(중국)";
management.lang_croatian="Croatian 크로아티아어";
management.lang_czech="Czech 체코어";
management.lang_dutch="Dutch 네델란드어";
management.lang_portuguese_braz="Portuguese (brazilian) 브라질 포르투칼어";
management.lang_english="English 영어";
management.lang_polish="Polish 폴란드어";
management.lang_french="French 프랑스어";
management.lang_german="German 독일어";
management.lang_turkish="Turkish 터키어";
management.lang_italian="Italian 이탈리아어";
management.lang_brazilian="Brazilian 브라질어";
management.lang_russian="Russian 러시아어";
management.lang_romanian="Romanian 루마니아어";
management.lang_slovenian="Slovenian 슬로바키아어";
management.lang_spanish="Spanish 스페인어";
management.lang_serbian="Serbian 세르비아어";
management.lang_swedish="Swedish 스웨덴어";
management.lang_japanese="Japanese 일본어";
management.lang_hungarian="Hungarian 헝가리어";
management.lang_latvian="Latvian 라트비아어";
management.lang_korean="Korean 한글";
management.net_legend="IP 필터 설정 (P2P 어플리케이션 용)";
management.net_conctrl="TCP 정체 제어";
management.net_port="최대 할당 Port 수";
management.net_tcptimeout="TCP Timeout (초)";
management.net_udptimeout="UDP Timeout (초)";
management.clock_legend="Overclocking";
management.clock_frq="주파수";
management.clock_support="지원하지 않습니다";
management.mmc_legend="MMC/SD Card 지원";
management.mmc_srv="MMC 장치";
management.mmc_gpiosel="GPIO 핀 번호 선택";
management.mmc_gpiopins="GPIO pins";
management.mmc_cardinfo="Card Info";
management.samba_legend="CIFS 자동 마운트(mount)";
management.samba_srv="Common Internet File System";
management.samba_share="파일 공유";
management.samba_stscript="Startscript";
management.SIPatH_srv="SIPatH";
management.SIPatH_port="SIP 포트";
management.SIPatH_domain="SIP 도메인";
management.gui_style="Router GUI Style(공유기 인터페이스 스타일)";

//help container
hmanagement.right1="자동 갱신:";
hmanagement.right2="웹화면 상태표시를 장기적으로 갱신합니다, 0 으로 설정하시면 자동갱신을 실시하지 않습니다.";

// ************ Port_Services.asp (used by Filters.asp and QoS.asp, QOSPort_Services.asp not used anymore) *****************************************//
portserv.titl="Port Services";
portserv.h2="Port Services";


// ** Networking.asp **//
networking.h2="VLAN Tagging";
networking.legend="Tagging";
networking.h22="Bridging";
networking.legend2="Bridge 작성";
networking.legend3="Bridge 할당";
networking.legend4="현재 Bridging Table";
networking.brname="Bridge Name";
networking.stp="STP(Spanning Tree) 유효화";
networking.iface="인터페이스";
networking.h5="DHCPD";
networking.legend5="Multiple DHCP Server";

//help container
hnetworking.right1="멀티 DHCPD";
hnetworking.right2="멀티 DHCPD를 사용하기위해서는, DNSMasq를 DHCP 서버로 설정을 하여야 합니다.";

// ** QoS.asp **//
qos.titl="Quality of Service(QOS)";
qos.h2="Quality Of Service (QoS) 설정";
qos.legend="QoS 설정";
qos.srv="QoS 기능 기동";
qos.type="Packet Scheduler(스케쥴방식)";
qos.aqd="Queueing Discipline(대기행렬규칙)";
qos.aqd_sfq="SFQ";
qos.aqd_codel="CODEL";
qos.aqd_fqcodel="FQ_CODEL";
qos.uplink="Uplink 통신 속도 (kbps)";
qos.dnlink="Downlink 통신 속도 (kbps)";
qos.legend2="서비스 우선순위 설정";
qos.prio_m="수동";
qos.prio_x="Exempt(제외)";
qos.prio_p="Premium(프리미엄)";
qos.prio_e="Express(익스프레스)";
qos.prio_b="Bulk(벌크)";
qos.legend3="Netmask 우선순위 설정";
qos.ipmask="IP/Mask";
qos.maxrate_b="최대 통신 속도 (kbit)";
qos.maxuprate_b="WAN Max Up(인터넷 최대 업로드 속도)";
qos.maxdownrate_b="WAN Max Down(인터네서 최대 다운로드 속도)";
qos.maxlanrate_b="LAN 최대 전송 속도";
qos.maxrate_o="최대 전송 속도";
qos.legend4="MAC 어드레스 우선순위";
qos.legend5="Ethernet Port 우선순위 설정";
qos.legend6="초기 대역폭 레벨";
qos.legend7="TCP-Packet 우선순위";
qos.pktdesc="다음의 Flag를 따라 Small TCP-packet의 우선순위를 설정합니다:";
qos.pktack="ACK";
qos.pktrst="RST";
qos.pktsyn="SYN";
qos.pktfin="FIN";
qos.enabledefaultlvls="사용자별 초기 제한값을 유효화 합니다";
qos.bandwidth="할당된 대역 (kbit)";

//help container
hqos.right1="";
hqos.right2="LAN 측 → 인터넷 측의 통신에 허용되는 대역폭을 지정합니다. Uplink:<br>80%-95% (최대) 의 범위에서 지정하십시요.<br>Downlink:<br>80%-100% (최대)의 범위내에서 다운로드를 설정하여 주십시요.";
hqos.right3="";
hqos.right4="사용자당 초기 할당값을 유효화 합니다.:<br>각각의 사용자별 혹은 모든 유저를 대상으로 초기 설정 레벨을 유효화 합니다.";
hqos.right6="서비스(어플리케이션)에서 이용가능한 최대 대역을 지정하실 수 있습니다.";
hqos.right8="부여받은 IP address 와 IP 범위내에서 모든 트레픽의 우선순위를 지정하실 수 있습니다.";
hqos.right10="우선순위를 지정하거나, MAC 어드레스를 입력함으로써 혹은 장치에 장치명을 부여함으로써 네트워크 상의 장치로부터의 모든 트래픽의 우선순위를 지정하실 수 있습니다.";
hqos.right12="장치가 접속되어져 있는 물리적 LAN 포트에 따라 통신속도를 제어 하실수 있습니다. LAN 포트 1~4번 포트에 접속되어져 있는 장치에 따라 우선 순위를 부여할 수 있습니다.";


// ** RouteTable.asp **//
routetbl.titl="Routing Table";
routetbl.h2="Routing Table Entry List";
routetbl.th1="목적지 LAN NET";


// ** Routing.asp **//
route.titl="경로 설정";
route.h2="고급자 경로 설정";
route.metric="Metric";
route.flags="Flags";
route.mod="동작 모드";
route.bgp_legend="BGP 설정";
route.bgp_ip="Neighbor IP 어드레스";
route.bgp_own_as="BGP Own AS#(자기 조직의 AS번호)";
route.bgp_as="Neighbor AS#";
route.rip2_mod="RIP2 Router";
route.olsrd_mod="OLSR Router";
route.olsrd_legend="OLSR (Optimized Link State Routing) 경로 설정";
route.olsrd_poll="Poll Rate(초)";
route.olsrd_gateway="Gateway Mode";
route.olsrd_hna="Host Net Announce";
route.olsrd_tc="TC Redundancy(중복/잉여)";
route.olsrd_mpr="MPR Coverage";
route.olsrd_lqfe="Link Quality Fish Eye";
route.olsrd_lqag="Link Quality Aging";
route.olsrd_lqdmin="Link Quality Dijkstra Min";
route.olsrd_lqdmax="Link Quality Dijkstra Max";
route.olsrd_lqlvl="링크 품질 레벨";
route.olsrd_hysteresis="Hysteresis(이력현상)";
route.olsrd_newiface="새 인터페이스";
route.olsrd_smartgw="Smart Gateway";
route.zebra_legend="Zebra 설정";
route.zebra_log="Zebra Log";
route.zebra_copt="Zebra 설정 스타일";
route.bird_legend="Bird 설정";
route.bird_log="Bird Log";
route.bird_copt="Bird 설정 스타일";
route.ospf_mod="OSPF Router";
route.ospf_legend="OSPF 경로 설정";
route.ospf_conf="OSPF 설정";
route.ospf_copt="OSPF 설정 스타일";
route.copt_gui="GUI";
route.copt_vtysh="Vtysh";
route.gateway_legend="동적 경로 사용 인터페이스";
route.static_legend="정적(고정) 경로 등록";
route.static_setno="설치 번호 지정";
route.static_name="공유기 이름";
route.static_ip="목적지 LAN NET";

//help container
hroute.right2="만일 이 공유기가 인테넷 접속을 호스팅하고 있는 경우, <em>Gateway</em> 모드를 선택하여 주십시요. 만일 다른 공유기가 네트워크 상에 존재하고 있는 경우 <em>Router</em> 모드를 선택하여 주십시요.";
hroute.right4="경로마다 번호가 지정되어 있습니다. 50가지의 경로까지 설정하실 수 있습니다.";
hroute.right6="이 공유기에 할당할 이름을 입력하여 주십시요.";
hroute.right8="정적 경로에 등록할 호스트의 네트워크 어드레스를 입력하여 주십시요.";
hroute.right10="목적지의 네트워크 어드레스의 서브넷 마스크를 입력하여 주십시요.";


// ** Site_Survey.asp **//
survey.titl="Site 조사";
survey.h2="인접 무선 네트워크";
survey.thjoin="사이트에 들어가기";


// ** Services.asp **//
service.titl="서비스";
service.h2="서비스 설정/관리";

service.apserv_legend="APServ 리모트 환경 설정";
service.apserv="APServ";

//kaid
service.kaid_legend="XBOX Kaid";
service.kaid_srv="Kaid 서비스 시작";
service.kaid_locdevnum="LAN측에서 사용하고 있는 장치수";
service.kaid_uibind="UI 통신에 사용하는 포트";
service.kaid_orbport="ORB 포트";
service.kaid_orbdeepport="ORB Deep 포트";

//DHCPd
service.dhcp_legend="DHCP 클라이언트";
service.dhcp_vendor="Set Vendorclass";
service.dhcp_reqip="요구하는 IP 어드레스 지정";
service.dhcp_legend2="DHCP 서버";
service.dhcp_srv="DHCP 서비스";
service.dhcp_jffs2="Use JFFS2 for client lease DB";
service.dhcp_nvramlease="Use NVRAM for client lease DB";
service.dhcp_domain="Domain 네임 취득";
service.dhcp_landomain="LAN Domain 네임 수동 설정";
service.dhcp_option="DHCP 서비스 추가 옵션";
service.dnsmasq_legend="DNSMasq";
service.dnsmasq_srv="DNSMasq";
service.dnsmasq_loc="Local DNS 기능";
service.dnsmasq_no_dns_rebind="No DNS Rebind";
service.dnsmasq_opt=" DNSMasq 추가 옵션";

//pptp.webservices
service.pptp_legend="PPTP";
service.pptp_srv="PPTP 서버 설정";
service.pptp_client="Client IP(s)";
service.pptp_chap="CHAP-Secrets";

//syslog.webservices
service.syslog_legend="System Log";
service.syslog_srv="Syslog 송수신 서비스";
service.syslog_ip="전송 서버";

//telnet.webservices
service.telnet_legend="Telnet";
service.telnet_srv="Telnet 서비스";

//pptpd_client.webservices
service.pptpd_legend="PPTP Client 설정";
service.pptpd_option="PPTP Client 옵션";
service.pptpd_ipdns="서버 IP 혹은 DNS Name";
service.pptpd_subnet="Remote Subnet(접속 상대측 네트워크 어드레스)";
service.pptpd_subnetmask="Remote Subnet Mask";
service.pptpd_encry="MPPE 임호화";
service.pptpd_mtu="MTU";
service.pptpd_mru="MRU";
service.pptpd_nat="NAT";
service.dns1="DNS1";
service.dns2="DNS2";
service.wins1="WINS1";
service.wins2="WINS2";

//rflow.webservices
service.rflow_legend="RFlow / MACupd";
service.rflow_srv1="RFlow";
service.rflow_srv2="MACupd";

//pppoe-relay.webservices
service.pppoe_legend="PPPoE Relay";
service.pppoe_srv="Relay";

//pppoe-server.webservices
service.pppoesrv_legend="PPPoE 서버 설정";
service.pppoesrv_srv="RP-PPPoE 서버 서비스";
service.pppoesrv_interface="RP-PPPoE 서버 인터페이스";
service.pppoesrv_srvopt="RP-PPPoE 서버 옵션";
service.pppoesrv_compr="압축";
service.pppoesrv_lcpei="LCP Echo 간격";
service.pppoesrv_lcpef="LCP Echo 실패";
service.pppoesrv_limit="Session Limit per MAC (맥어드레스 당 기한 제한)";
service.pppoesrv_idlet="Client 대기 시간";
service.pppoesrv_auth="인증";
service.pppoesrv_radip="Radius 서버 IP 어드레스";
service.pppoesrv_radauthport="Radius 인증 포트";
service.pppoesrv_radaccport="Radius Accounting Port";
service.pppoesrv_radkey="Radius 공유키";
service.pppoesrv_chaps="Local User 관리 (CHAP Secrets)";

//help container
hpppoesrv.right2="IP: 0.0.0.0 -> 형식에 의거하여 IP어드레스를 정하실 수 있습니다";
hpppoesrv.right3="IP 어드레스의 범위에 의거하여 유효한 클라이언트 수를 설정하셔야 합니다.";

//snmp.webservices
service.snmp_legend="SNMP";
service.snmp_srv="SNMP";
service.snmp_loc="위치";
service.snmp_contact="연락처";
service.snmp_name="이름";
service.snmp_read="RO Community";
service.snmp_write="RW Community";

//openvpn.webvpn
service.vpnd_legend="OpenVPN Server/Daemon";
service.vpnd_srv="OpenVPN";
service.vpnd_starttype="Start Type";
service.vpnd_startWanup="WAN Up";
service.vpnd_startSystem="시스템";
service.vpnd_crl="Revoke 리스트 인증";
service.vpnd_config="추가 설정";
service.vpnd_dhpem="DH PEM";
service.vpnd_tlsauth="TLS 인증키";
service.vpnd_cert="Public 서버 인증";
service.vpnd_key="Private(사설 서버) 키";
service.vpnd_pkcs="PKCS12 Key";
service.vpnd_mode="서버 모드";
service.vpnd_net="네트워크";
service.vpnd_mask="Netmask";
service.vpnd_startip="Pool start IP";
service.vpnd_endip="Pool end IP";
service.vpnd_cl2cl="단말에서 단말로의 접속 허가";
service.vpnd_switch="Config as";
service.vpnd_dupcn="Allow duplicate cn";
service.vpnd_proxy="DHCP-Proxy mode";
service.vpnd_clcon="Client connect script";
service.vpnd_ccddef="CCD-Dir DEFAULT file";
service.vpnd_dhcpbl="Tunnel을 교차하여 DHCP 차단";
service.vpnd_static="Static Key";
service.vpn_redirgate="초기설정된 Gateway 로 경로 재설정";
service.vpn_legend="OpenVPN Client";
service.vpn_srv="Start OpenVPN Client";
service.vpn_ipname="서버 IP 어드레스/서버 이름";
service.vpn_mtu="Tunnel MTU 설정";
service.vpn_mss="Tunnel UDP MSS-Fix";
service.vpn_fragment="Tunnel UDP Fragment(세분화)";
service.vpn_compress="LZO 압축사용";
service.vpn_cl2cl="lient 에서 Client로 접속 허가";
service.vpn_tunnel="Tunnel 프로토콜";
service.vpn_tuntap="Tunnel 장치";
service.vpn_srvcert="공개 서버 증명서";
service.vpn_clicert="공개 클라이언트 증명서";
service.vpn_certtype="nsCertType verification";
service.vpn_clikey="Private Client Key(사설 클라이언트 키)";
service.vpn_nat="NAT";
service.vpn_cipher="Cipher 암호화";
service.vpn_auth="Hash 알고리즘";
service.vpn_bridge="TAP 에서 br0으로 중계";
service.vpn_adv="고급 옵션";
service.vpn_tlscip="TLS Cipher";
service.vpn_route="사용자 정의에 따른 경로 설정";

//help container
hstatus_vpn.right1="사용자 정의에 의한 경로 설정:<br><i>IPs/NETs 를 0.0.0.0/0 부터 추가하여 클라이언트가 강제로 초기 설정된 Gateway처럼 터널을 사용하도록 함. IP/NET 당 한 라인.<br><i>IP Address/Netmask:</i><br>DHCP-Proxy 모드를 사용할 경우 반드시 설정이 되어야 하며, local TAP은 중계(bridged)되어 있어서는 않됩니다.</i>";
hstatus_vpn.right2="추가설정:<br><i>공유기에서 클라이언트가 \'push \"route IP mask gateway\"\'추가하도록 하고, DNS/WINS가 \'push \"dhcp-option DNS (혹은 WINS) IP\"\'를 설정에 추가하도록 합니다.</i><br>클라이언트의 직접 접속:<br><i>USB 혹은 JFFS 가 /jffs에 마운드되어 있을 경우, 스크립트는 /jffs/etc/openvpn/ccd/에서 호출되어져야 합니다</i>";
hstatus_vpn.right3="개괄:<br><i> pkcs12 (+dh on server), static, 표준인증등의 3 가지의 인증방법을 지원합니다, MSS 링크의 한쪽만의 MSS를 유효화 하여 주시고, 링크 양쪽의 프래그먼트를 유효화 하여 주십시요.</i>";

//vnc.repeater
service.vncrepeater_legend="VNC";
service.vncrepeater="VNC 리피터(Repeater)";

//sshd.webservices
service.ssh_legend="Secure Shell";
service.ssh_srv="SSH 서비스";
service.ssh_password="로그인시 사용자 암호 사용";
service.ssh_key="인증키";
service.ssh_forwarding="SSH TCP Forwarding";

//radiooff.webservices
service.radiooff_legend="SES / AOSS / EZ-SETUP / WPS 버튼";
service.radiooff_legend_aoss="AOSS 버튼 기능";
service.radiooff_srv="Turning off radio";
service.radiooff_srv_aoss="AOSS";
service.radiooff_srv_disabled="사용안함";
service.radiooff_bootoff="기동시 무선 기능을 무효화함";

//ses.webservices ====> might replace the above radiooff_button
service.ses_legend="SES / AOSS / EZ-SETUP / WPS 버튼";
service.ses_srv="Button(버튼) 동작";
service.ses_toggleradio="무선 기능 전환";
service.ses_script="수동 스크립트";

//hwmon.webservices
service.hwmon_legend="하드웨어 모니터링(감시)";
service.hwmon_critemp="임계 상한 온도 (FAN 동작 시작)";
service.hwmon_hystemp="임계 하한 온도 (FAN 동작 정지)";

//rstat.webservices
service.rstats_legend="대역폭 모니터링(감시)";
service.rstats_srv="rstats 서비스";
service.rstats_path="대역 데이터 보존";
service.rstats_time="보존가격";
service.rstats_usrdir="사용자 디렉토리";

//nstx.webservices
service.nstx_legend="IP over DNS Tunneling";
service.nstx_srv="nstx Daemon";
service.nstx_ipenable="Bind to this IP only";
service.nstx_log="디버그 메세지 교환";

//ttraff.webservices
service.ttraff_legend="WAN Traffic Counter";
service.ttraff_daemon="ttraff Daemon";

//notifier.webservices
service.warn_legend="접속 경고 통지";
service.warn="경고 통지";
service.warn_limit="접속제한";
service.warn_server="Email SMTP 서버";
service.warn_from="발신자 Email 주소";
service.warn_fromfull="발신자 성명";
service.warn_to="수신자 Email 주소";
service.warn_domain="수신자 Domain 네임";
service.warn_user="SMTP 인증 사용자 이름";
service.warn_pass="SMTP 인증 사용자 암호";

//milkfish.webservices
service.milkfish_siprouter="Milkfish SIP 라우터";
service.milkfish_alias="Alias";
service.milkfish_uri="SIP URI";
service.milkfish_mainswitch="SIP 라우터";
service.milkfish_fromswitch="From-Substitution(대리)";
service.milkfish_fromdomain="From-Domain";
service.milkfish_username="Milkfish 사용자 이름";
service.milkfish_password="Milkfish 사용자 암호";
service.milkfish_audit="Milkfish 심사";
service.milkfish_siptrace="SIP Trace(추적)";
service.milkfish_subscribers="Local Subscribers(가입자)";
service.milkfish_aliases="Local Aliases(가명)";
service.milkfish_dynsip="변동 SIP";
service.milkfish_status="SIP 상태";
service.milkfish_database="SIP 데이터베이스";
service.milkfish_messaging="SIP 메세지";
service.milkfish_phonebook="SIP 전화번호부";
service.milkfish_dynsipdomain="DynSIP 도메인";
service.milkfish_dynsipurl="DynSIP Update URL";
service.milkfish_dsusername="DynSIP 사용자 이름";
service.milkfish_dspassword="DynSIP 사용자 암호";
service.milkfish_sipmessage="SIP 메세지";
service.milkfish_destination="SIP Destination";
service.milkfish_contact="연락처";
service.milkfish_agent="User Agent";
service.milkfish_registrations="등록 활성화";
//service.milkfish_="";//"
service.hmilkfish_right2="Milkfish SIP 라우터를 활성화 혹은 비활성화 합니다.";
service.hmilkfish_right4="다음의 경우 설정을 활성화 혹은 비활성화 합니다:-발신 SIP 메세지에서 WAN IP 어드레스의 헤더필드를 대신합니다. 만일 WAN IP 어드레스가 변경되었을 시, 피호출자로부터 회신을 허락할 경우 이 설정을 활성화 시켜야 합니다.";
service.hmilkfish_right6="피호출자가 이 도메인으로 발신을 할 경우(현재 사용자의 WAN IP 어드레스를 대신하여), 호출자가 WAN IP 어드레스를 대신하여 도메인으로 부터 발송할 경우. From-Substitution은 이 설정이 유효하도록 활성화 시켜야 합니다.";
service.hmilkfish_right8="Milkfish 커뮤니티 포럼의 사용자 이름을 이곳에 입력하십시요. yourname.homesip.net에 등록하십시요.";
service.hmilkfish_right10="Milkfish 커뮤니티 포럼의 사용자 암호를 이곳에 입력하십시요. yourname.homesip.net에 등록하여 주십시요.";
service.hmilkfish_right12="공유기에 기본적인 SIP tracing을 활성화 혹은 비활성화 합니다.";
service.hmilkfish_right14="Local SIP 신청자는 현지에서 SIP 계정이 관리되어집니다..";
service.hmilkfish_right16="가명의 Local SIP은 Email 전송과 유사하게 SIP을 전송합니다. alphanumeric-to-numeric(알파벳&숫자 조합 번호를 숫자로 변환하는) 전화의 사용자 계정 혹은 vice versa를 통하여 전송되어 집니다.";
service.hmilkfish_right18="변동 SIP를 할성화 혹은 비활성화시킵니다 (예. Homesip.net service). 사용자 이름과 암호를 설정하셔야 합니다.";
service.hmilkfish_right20="정의되지 않은 영역입니다 - 비워두시길 바랍니다";
service.hmilkfish_right22="정의되지 않은 영역입니다 - 비워두시길 바랍니다";
service.hmilkfish_right24="정의되지 않은 영역입니다 - 비워두시길 바랍니다";
service.hmilkfish_right26="정의되지 않은 영역입니다 - 비워두시길 바랍니다";
//service.hmilkfish_="";//"

service.samba3_srv="Samba";
service.samba3_srvstr="Server String";
service.samba3_pub="공유 공개";
service.samba3_config="수동 환경 설정";
service.samba3_workgrp="Workgroup";
service.samba3_dirpath="Path to Files";
service.samba3_usr1="사용자 1";
service.samba3_pass1=" 암호 1";
service.samba3_usr2="사용자 2";
service.samba3_pass2=" 암호 2";
service.samba3_pubacl="읽기만 가능";
service.samba3_advanced="상급자 서비스";
service.samba3_custom="수동 환경설정 사용";
service.samba3_shares="공유";
service.samba3_share_path="Path";
service.samba3_share_label="이름";
service.samba3_share_public="Public(공개)";
service.samba3_share_access="Access";
service.samba3_users="사용자";
service.samba3_username="사용자 이름";
service.samba3_password="사용자 암호";
service.samba3_user_shares="접근 공유";
service.dlna_type_audio="Audio(음악)";
service.dlna_type_video="Video(영상)";
service.dlna_type_images="이미지(그림)";

// ** eop-tunnel.asp **//
eoip.titl="EoIP Tunnel";
eoip.tunnel="Tunnel";
eoip.legend="Ethernet Over IP Tunneling";
eoip.srv="EoIP Tunnel";
eoip.remoteIP="리모트 IP 어드레스";
eoip.tunnelID="Tunnel ID";
eoip.comp="압축";
eoip.passtos="TOS 통과";
eoip.frag="fragment";
eoip.mssfix="mssfix";
eoip.shaper="shaper";
eoip.bridging="Bridging";


// ** Sipath.asp + cgi **//
sipath.titl="SiPath 개요";
sipath.phone_titl="전화번호부";
sipath.status_titl="상태";


// ** Status_Lan.asp **//
status_lan.titl="LAN 상태";
status_lan.h2="Local(LAN측) 네트워크";
status_lan.legend="LAN 상태";
status_lan.h22="Dynamic Host Configuration Protocol(DHCP서버/클라이언트)";
status_lan.legend2="DHCP 서버";
status_lan.legend3="DHCP 클라이언트(단말)";
status_lan.legend4="이용중인 클라이언트(단말)";
status_lan.legend5="접속된 PPTP Clients";
status_lan.legend6="접속된 PPPOE Clients";
status_lan.concount="접속수";
status_lan.conratio="이용율";

//help container
hstatus_lan.right2="이 기기의 유선 LAN에 할당된 MAC 어드레스를 표시합니다.";
hstatus_lan.right4="이 기기의 LAN측(로컬네트워크) IP 어드레스를 표시합니다.";
hstatus_lan.right6="이 기기의 LAN측(로컬네트워크)의 subnet mask를 표시합니다.";
hstatus_lan.right8="이 기기가 DHCP 서버로서 동작을 하고 있을 경우, 동작 정보가 표시되어 집니다.";
hstatus_lan.right10="클라이언트 표시부의 MAC 주소를 클릭하여 IEEE의 OUI (Organizationally Unique Identifier)에 대한 정보를 참조 할 수 있습니다.";


// ** Status_Bandwidth.asp **//
status_band.titl="대역폭 모니터링(성능)";
status_band.h2="대역폭 모니터링";
status_band.chg_unit="Switch to ";
status_band.chg_scale="Autoscale";
status_band.chg_error="인터페이스 정보를 가져올 수 없습니다.";
status_band.chg_collect_initial="초기화 진행중입니다, 잠시만 기다려 주십시요...";
status_band.strin="수신";
status_band.strout="송신";
status_band.follow="follow";
status_band.up="up";

//help container
hstatus_band.svg="대역폭 그래프를 표시하기 위해서 Adobe's SVG 플러그인이 필요합니다.";
hstatus_band.right1="단위를 변경하는 것이 가능합니다. (bytes/s 혹은 bits/s).";
hstatus_band.right2="대역축 기준을 변경하실 수 있습니다.";

// ** Status_Router.asp **//
status_router.titl="기기상태";
status_router.h2="장비 정보";
status_router.legend="시스템";
status_router.sys_model="공유기 모델";
status_router.sys_firmver="펌웨어 버전";
status_router.sys_time="현재 시간";
status_router.sys_up="가동시간";
status_router.sys_load="CPU 이용율";
status_router.sys_kernel="Kernel 버전";
status_router.legend2="CPU";
status_router.cpu="CPU 모델";
status_router.clock="CPU 클럭";
status_router.legend3="메모리";
status_router.mem_tot="전체 용량";
status_router.mem_free="사용가능한 메모리 용량";
status_router.mem_used="사용중인 메모리 용량";
status_router.mem_buf="버퍼";
status_router.mem_cached="캐쉬이용";
status_router.mem_active="활성화";
status_router.mem_inactive="비활성화";
status_router.mem_hidden="Hidden"; // do not translate this line, this is bogus (BrainSlayer)"
status_router.legend4="네트워크";
status_router.net_maxports="IP 필터 최대 Port 수";
status_router.net_conntrack="사용중인 IP 접속수";
status_router.notavail="사용할 수 없습니다.";
status_router.legend6="비어있는 공간";
status_router.inpvolt="입력 전압";
status_router.cputemp="CPU 온도";

//help container
hstatus_router.right2="<i>Setup</i> 탭에서 설정하신 기기에 부여된 이름입니다.";
hstatus_router.right4="인터넷 측에서 사용되는 MAC 주소입니다, ISP에서 MAC 어드레스를 식별하는 데 사용되어집니다.";
hstatus_router.right6="현재 이 기기에 적용되어진 펌웨어 정보입니다.";
hstatus_router.right8="페이지에서 설정된 NTP 서버에서 가져온 현재 시간을 표시합니다. <em>" + bmenu.setup + " | " + bmenu.setupbasic + "</em>.";
hstatus_router.right10="기기가 기동을 시작한 시점부터의 시간입니다";
hstatus_router.right12="CPU 사용률을 표시합니다. 각각 1 분, 5 분, 15 분 각각 동안에, CPU 평균 가동률이 표시됩니다.";

// ** Status_Internet.asp **//
status_inet.titl="WAN 상태";
status_inet.h11="WAN";
status_inet.conft="인터넷 접속 방식";
status_inet.www_loginstatus="로그인 상태";
status_inet.wanuptime="접속시간";
status_inet.leasetime="남아있는 Lease Time";
status_inet.traff="트래픽";
status_inet.traff_tot="트래픽 합계";
status_inet.traff_mon="월별 트래픽";
status_inet.traffin="수신";
status_inet.traffout="송신";
status_inet.previous="전월";
status_inet.next="다음달";
status_inet.dataadmin="Data 관리";
status_inet.delete_confirm="!!경고!! 모든 트래픽 데이터가 삭제됩니다. 진행하시겠습니까?";


//help container
hstatus_inet.right2="ISP (인터넷)에 연결하는 방법을 표시합니다. 이 정보는 설정 페이지에서 설정할 수 있습니다. <em> Connect </ em>, <em> Disconnect </ em> 버튼을 클릭하여 연결을 시작 혹은 끊을 수 있습니다.";
hstatus_inet.right4="마지막 재기동시부터 인터넷 트래픽을 표시합니다.";
hstatus_inet.right6="공유기의 월별 인터넷 트래픽을 볼 수 있습니다. 마우스를 그래픽 위쪽으로 드레그하시면 일별 데이터를 볼수 있습니다. 데이터는 nvram에 저장되어집니다.";


// ** Status_Conntrack.asp **//
status_conn.titl="접속중인 IP 어드레스 테이블";
status_conn.h2="접속중인 IP 어드레스";


// ** Status_SputnikAPD.asp **//
status_sputnik.titl="Sputnik Agent 정보";
status_sputnik.h2="Sputnik&reg; Agent&trade;";
status_sputnik.manage="관리자";
status_sputnik.license="SCC 라이센스 No.";

//help container
hstatus_sputnik.right1="Sputnik Agent 정보";
hstatus_sputnik.right2="Sputnik Agent process에 관한 정보를 표시합니다.";
hstatus_sputnik.right4="이 기기가 접속하는 Sputnik 제어 센터를 표시합니다.";
hstatus_sputnik.right6="현재의 Agent 상태를 나타닙니다.";
hstatus_sputnik.right8="Sputnik 제어 센터의 접속에 사용되는 라이센스 번호를 표시합니다.";


// ** Status_Wireless.asp **//
status_wireless.titl="Wireless 상태";
status_wireless.h2="Wireless";
status_wireless.legend="Wireless 상태";
status_wireless.net="네트워크";
status_wireless.pptp="PPTP 상태";
status_wireless.legend2="무선 패킷 정보";
status_wireless.rx="수신 (RX)";
status_wireless.tx="송신 (TX)";
status_wireless.h22="무선LAN클라이언트";
status_wireless.legend3="Clients";
status_wireless.signal_qual="신호품질";
status_wireless.wds="WDS Nodes(접속상대)";

// ** GPS info **//
status_gpsi.legend="GPS 정보";
status_gpsi.status="상태";
status_gpsi.lon="경도";
status_gpsi.lat="위도";
status_gpsi.alt="고도";
status_gpsi.sat="검색되어진 위성";

//help container
hstatus_wireless.right2="이 장비의 무선 LAN에 사용되는 MAC 주소를 표시합니다.";
hstatus_wireless.right4="무선 LAN 설정에서 설정된 PHY 모드 (Mixed, G-Only, B-Only 혹은 무효화)가 표시됩니다.";


// ** Status_OpenVPN.asp **//
status_openvpn.titl="OpenVPN 상태";


// ** Triggering.asp **//
trforward.titl="Port Triggering(어드레스 변환/Trigger지정)";
trforward.h2="Port Triggering(Trigger지정의 어드레스 변환 설정)";
trforward.legend="변환 룰";
trforward.trrange="Triggered 포트 범위(LAN->인터넷)";
trforward.fwdrange="전송포트 범위(인터넷->LAN)";
trforward.app="어플리케이션 이름";

//help container
htrforward.right2="trigger에 적용/신청하신 이름을 입력하여 주십시요.";
htrforward.right4="Trigger되는 포트를 지정합니다. LAN 측에서 이 포트에 대한 통신이 발생한 경우 전송 포트를 이용한 통신을 할 수 있습니다. 어떤 포트를 지정해야 하는가에 대해서는 사용하시는 응용 프로그램의 설명서 등을 참조하십시오.";
htrforward.right6="Trigger에 의해 전송되는 Port의 범위를 지정합니다. 어떤 포트를 지정 해야할지 등의 내용은 사용하는 응용 프로그램의 설명서 등을 참조하십시오.";
htrforward.right8="Trigger와 Forwarded Range의 포트의 시작 포트를 지정하여 주십시요.";
htrforward.right10="Trigger와 Forwarded Range의 종료 포트를 지정하여 주십시요.";


// ** Upgrade.asp **//
upgrad.titl="펌웨어 업그레이드";
upgrad.h2="펌웨어 관리";
upgrad.legend="펌웨어 업그레이드";
upgrad.info1="갱신후, 재설정하십시요";
upgrad.resetOff="리셋/재설정 하지 마십시요";
upgrad.resetOn="공장 출하상태로 재설정";
upgrad.file="갱신을 진행할 파일을 선택하여 주십시요";
upgrad.warning="!!!경고!!!";
upgrad.mess1="펌웨어 업데이트는 보통 몇 분 정도 걸립니다. <br /> 업데이트 중 전원을 끄거나 리셋 버튼을 누르지 마십시오!";

//help container
hupgrad.right2="펌웨어 업데이트합니다. <em>  Browse(검색)... </ em> 버튼을 클릭하여 공유기에 업로드할 펌웨어 파일을 선택하십시요. <br /> <br /> <em> Upgrade(업그레이드) </ em>을 클릭하면 업데이트가 시작됩니다. 일단 시작된 업데이트는 도중에 중단 할 수 없습니다.";


// ** UPnP.asp **//
upnp.titl="UPnP";
upnp.h2="Universal Plug and Play (UPnP설정)";
upnp.legend="Forwards(포트교환 리스트)";
upnp.legend2="UPnP 설정";
upnp.serv="UPnP 서비스";
upnp.clear="기동시 모든 Port Forwards 리스트 삭제";
upnp.url="presentation URL 발송";
upnp.msg1="클릭하시면 Entry가 삭제됩니다";
upnp.msg2="모든 Entry를 삭제할까요?";

//help container
hupnp.right2="Trash can(휴지통)을 클릭하시면 등록된 Entry를 삭제하실 수 있습니다.";
hupnp.right4="어플리케이션이 자동으로 Port Forwarding의 환경 설정의 변경을 허락합니다.";


// ** VPN.asp **//
vpn.titl="VPN Passthrough(통과)";
vpn.h2="Virtual Private Network(가상 사설 네트워크) (VPN 설정)";
vpn.legend="VPN Passthrough(통과)";
vpn.ipsec="IPSec Passthrough(통과)";
vpn.pptp="PPTP Passthrough(통과)";
vpn.l2tp="L2TP Passthrough(통과)";

//help container
hvpn.right1="이 기능을 사용하여 IPSec, PPTP 그리고/혹은 L2TP passthrough의 활성화를 선택하시면, LAN 쪽 네트워크 장비가 VPN 프로토콜을 사용하여 외부 서버와 통신 할 수 있습니다.";


// ** Vlan.asp **//
vlan.titl="Virtual LAN(가상랜)";
vlan.h2="Virtual Local Area Network (VLAN)";
vlan.legend="VLAN";
vlan.bridge="할당상대<br />Bridge";
vlan.tagged="Tagged";
vlan.negociate="Auto-Negotiate";
vlan.aggregation="Link Aggregation<br>on Ports 3 & 4";
vlan.trunk="Trunk";
vlan.linkstatus="연결 상태";

// ** WEP.asp **//
wep.defkey="초기설정 전송키(Transmit Key)";
wep.passphrase="Passphrase";


// ** WOL.asp **//
wol.titl="WOL";
wol.h2="Wake-On-LAN 설정";
wol.legend="검색된 Host";
wol.legend2="WOL 등록 리스트";
wol.legend3="Output";
wol.legend4="수동 WOL";
wol.enable="WOL 이용 가능";
wol.mac="MAC Address(es)";
wol.broadcast="Net Broadcast";
wol.udp="UDP 포트";
wol.msg1="클릭하시면 WOL host 가 삭제됩니다";
wol.h22="자동 Wake-On-LAN 설정";
wol.legend5="Wake-On-LAN daemon";
wol.srv="WOL daemon(자동기동)";
wol.pass="SecureOn 암호";

//help container
hwol.right2="Wake-On-Lan은 LAN 측 네트워크에 존재하는 호스트를 이 장비로부터 기동시키는 기능입니다. 이 기능을 사용하기 위해서는 기동시킬 장비 역시 Wake-On-Lan 에 대응하여야 합니다. <em>"+ sbutton.wol + "</ em>를 클릭하면 수동으로 지정된 장치를 기동시킬 수 있습니다. 또한 "+wol.srv+" 를 클릭하면 자동으로 클라이언트가 기동하도록 프로그램 할 수 있습니다.";
hwol.right4="MAC 어드레스는 xx : xx : xx : xx : xx : xx 의 형식으로 지정되어야 합니다 (예 : 01:23:45:67:89:AB). 또한 여러 MAC 주소를 지정하는 경우, 주소 사이에 공백을 넣어주십시요.";
hwol.right6="IP 주소는 일반적으로 브로드 캐스트(Broadcast) 주소로 지정해야 합니다. 그러나, LAN 측 네트워크에 존재하지 않는 경우는 Unicast 주소를 사용할 수 도 있습니다.";


// ** WanMAC.asp **//
wanmac.titl="MAC Address Clone";
wanmac.h2="MAC Address Clone";
wanmac.legend="MAC Clone";
wanmac.wan="Clone WAN MAC(다음의 MAC 어드레스를 인터넷측 포트에 사용)";
wanmac.wlan="Clone Wireless MAC(다음의 MAC 어드레스를 무선랜 측에 사용)";

//help container
hwanmac.right2="일부 ISP에는 접속 기기의 MAC 주소 등록이 필요한 경우가 있습니다. 여기에서는 ISP에 MAC 주소를 다시 등록하는 대신에 이 장비의 인터넷 측 주소에 등록 된 MAC을 설정하고, 통신을 할 수 있습니다.";


// ** WL_WPATable.asp / WPA.asp / Radius.asp **//
wpa.titl="무선 보안";
wpa.h2="무선 보안 설정";
wpa.secmode="Security Mode(무선랜 인증)";
wpa.legend="무선랜 암호화";
wpa.auth_mode="네트워크 인증";
wpa.wpa="WPA";
wpa.radius="Radius";
wpa.gtk_rekey="WPA Group Key 갱신 간격(초)";
wpa.rekey="Key 갱신 간격(초)";
wpa.radius_ipaddr="RADIUS Server Address";
wpa.radius_port="RADIUS Server Port";
wpa.radius_key="RADIUS Key";
wpa.algorithms="WPA 알고리즘";
wpa.shared_key="WPA 공유키";


aoss.titl="AOSS 보안";
aoss.aoss="AOSS";
aoss.service="AOSS 서비스";
aoss.enable="AOSS를 활성화 합니다";
aoss.start="AOSS를 시작합니다";
aoss.securitymodes="Security Modes";
aoss.wpaaes="WPA AES";
aoss.wpatkip="WPA TKIP";
aoss.wep="WEP 64/128";
aoss.client_name="클라이언트 이름";
aoss.security="Security 보안";
aoss.connectivity="접속 가능/불가능";
aoss.clients="AOSS 클라이언트";
aoss.notice="통지";
aoss.ap_mode_notice="통지: AOSS는 primary radio가 AP 혹은 WDS AP의 상태에서만 사용이 가능합니다.";
aoss.wep_notice="WEP 보안모드는 불안정하기때문에 WEP를 사용하는 것을 권하지 않습니다.";
aoss.wep_info="(AOSS를 지원하는 게임 콘솔과 접속을 할 경우 사용하십시요)";
aoss.wps="WPS 설정";
aoss.wps_ap_pin="WPS Gateway PIN (Label)";
aoss.wpspin="WPS 클라이언트 PIN";
aoss.wpsactivate="PIN 활성화";
aoss.wpsregister="PIN 등록";
aoss.wpsgenerate="PIN 생성";
aoss.pinnotvalid="유효하지 않은 PIN입니다, checksum(검사합계)이 맞지 않습니다.!";
aoss.wpsenable="WPS 버튼";
aoss.wpsstatus="WPS 상태";
aoss.externalregistrar="PIN Method";
aoss.release="개방";
aoss.configure="환경설정";

olupgrade.avail_updates="유효한 업데이트가 있습니다";
olupgrade.version="버전";
olupgrade.release="개방";
olupgrade.readme="추가 정보";
olupgrade.choose="선택";
olupgrade.retrieve_error="업데이트 정보를 검색하는 동안 에러가 발생했습니다";

nintendo.titl="Nintendo 닌텐도";
nintendo.spotpass.titl="Nintendo SpotPass";
nintendo.spotpass.enable="Nintendo SpotPass 우효화";
nintendo.spotpass.servers="서비스 허가";

sec80211x.xsuptype="XSupplicant Type(동작 타입)";
sec80211x.keyxchng="EAP Key-Management";
sec80211x.servercertif="Public Server Certificate(CA증명서)";
sec80211x.clientcertif="클라이언트 증명서";
sec80211x.phase2="Phase2";
sec80211x.anon="Anonymous Identity(익명신분)";
sec80211x.options="추가 네트워크 옵션";

//help container
hwpa.right2="무선 LAN 인터페이스의 보안을 해제, WEP, WPA Personal, WPA Enterprise, RADIUS 중에서 선택합니다. 동일한 네트워크상의 클라이언트는 모두 동일한 보안 방식이어야 합니다. 11N 모드에서는 반드시 WPA2/AES를 사용하십시요.";


// ** WL_FilterTable.asp **//
wl_filter.titl="MAC 어드레스 필터 리스트";
wl_filter.h2="MAC 어드레스 필터 리스트";
wl_filter.h3="MAC 어드리스는 다음과 같이 입력하여 주십시요&nbsp;:&nbsp;&nbsp;&nbsp;xx:xx:xx:xx:xx:xx";



// ** WL_ActiveTable.asp **//
wl_active.titl="무선랜 클라이언트 MAC 리스트";
wl_active.h2="무선랜 클라이언트 MAC 리스트";
wl_active.h3="MAC 필터 적용";
wl_active.active="접속중인 클라이언트";
wl_active.inactive="미접속인 클라이언트";


// ** Wireless_WDS.asp **//
wds.titl="WDS";
wds.h2="Wireless Distribution System(WDS 설정)";
wds.legend="WDS 설정";
wds.label="Lazy WDS";
wds.label2="WDS Subnet";
wds.wl_mac="Wireless MAC";
wds.lazy_default="초기설정: 사용안함";
wds.nat1="wLAN->WDS";
wds.nat2="WDS->wLAN";
wds.subnet="Subnet";
wds.legend2="상세 설정";


// ** Wireless_radauth.asp **//
radius.titl="Radius";
radius.h2="Remote Authentication Dial-In User Service";
radius.legend="Radius 인증";
radius.label="MAC Radius 클라이언트";
radius.label2="MAC 어드레스 형식";
radius.label3="Radius 인증서버 주소";
radius.label4="Radius 인증서버 포트";
radius.label7="Radius Auth Shared Secret";

radius.label23="Radius 인증 백업 서버 어드레스";
radius.label24="Radius 인증 백업 서버 포트";
radius.label27="Radius Auth Backup Shared Secret";

radius.label5="인증받지 않고 통신가능한 최대 상용자수";
radius.label6="인증 암호";
radius.label8="서버와 접속이 끊어졌을 경우 Radius 무효화";
radius.label13="Radius Acct Server Address";
radius.label14="Radius Acct Server Port";
radius.label17="Radius Acct Shared Secret";
radius.label18="Radius Accounting";

// ** Wireless_MAC.asp **//
wl_mac.titl="MAC 어드레스 필터";
wl_mac.h2="무선 MAC 어드레스 필터";
wl_mac.legend="MAC 어드레스 필터";
wl_mac.label="필터 사용";
wl_mac.label2="필터 모드";
wl_mac.deny="리스트에 등록되어 있는 클라이언트의 통신을 거부합니다";
wl_mac.allow="리스트에 등록되어 있는 클라이언트만 무선 네트워크의 접속을 허락합니다";

// ** WiMAX
wl_wimax.titl="WiMAX";
wl_wimax.h2="Worldwide Interoperability for Microwave Access";
wl_wimax.downstream="Downstream Frequency";
wl_wimax.upstream="Upstream Frequency";
wl_wimax.width="Channel Width";
wl_wimax.duplex="Duplex Mode";
wl_wimax.mode="Operation Mode";
wl_wimax.mac="Subscriber MAC Address";

// ** Gpio **//
gpio.titl="Gpio Inputs / Outputs";
gpio.h2="Gpio Inputs / Outputs";
gpio.oplegend="Gpio Outputs";
gpio.iplegend="Gpio Inputs";

// ** FreeRadius.asp **//
freeradius.titl="FreeRadius";
freeradius.h2="FreeRadius";
freeradius.certificate="서버 증명서";
freeradius.cert="증명서 작성";
freeradius.clientcert="클라이언트 증명서";
freeradius.settings="설정";
freeradius.users="사용자";
freeradius.clients="클라이언트";
freeradius.username="사용자 이름";
freeradius.password="사용자 암호";
freeradius.downstream="Downspeed";
freeradius.upstream="Upspeed";
freeradius.sharedkey="공유키";


freeradius.countrycode="국가 코드";
freeradius.state="주, 도시, 현 혹은 도";
freeradius.locality="지역";
freeradius.organisation="조직 / 회사";
freeradius.email="Email 주소";
freeradius.common="증명서명";
freeradius.expiration="유효기간(일자)";
freeradius.passphrase="Passphrase";
//freeradius.generate="증명서 생성";
freeradius.cert_status="증명서 상태";
freeradius.port="Radius 포트";

//help container
hfreeradius.right2="FreeRadius를 시작하기 전에 JFFS를 반드시 활성화 하여야 합니다.";

// ** Wireless_Advanced.asp **//
wl_adv.titl="고급 무선 설정";
wl_adv.h2="고급 무선 설정";
wl_adv.legend="고급 설정";
wl_adv.legend2="무선 멀티미디어 지원 설정";
wl_adv.label="802.11 인증 모드";
wl_adv.label2="Basic Rate";
wl_adv.label3="송신 Rate";
wl_adv.label4="CTS Protection(IEEE802.11g 보안 모드)";
wl_adv.label5="Frame Burst";
wl_adv.label6="Beacon 송신 간격";
wl_adv.label7="DTIM 송신 간격";
wl_adv.label8="Fragmentation Threshold";
wl_adv.label9="RTS Threshold";
wl_adv.label10="최대 동시 접속 대수";
wl_adv.label11="AP Isolation(프라이버시 세퍼레이트)";
wl_adv.label12="TX 송신 안테나";
wl_adv.label13="RX 수신 안테나";
wl_adv.label14="Preamble(PLCP 헤드)";
wl_adv.reference="Noise Reference";
wl_adv.label16="Afterburner";
wl_adv.label17="무선랜에서 설정화면으로 접속";
wl_adv.label18="Wifi WMM 지원";
wl_adv.label19="No-Acknowledgement";
wl_adv.label20="Shortslot Override";
wl_adv.label21="최대 전송율";
wl_adv.label23="최저 전송율";
wl_adv.label22="Bluetooth Coexistence 모드";
wl_adv.label24="Antenna Alignment";
wl_adv.label25="안테나 출력";
wl_adv.table1="EDCA AP Parameters (AP -> 클라이언트)";

wl_adv.txchainmask="TX 송신 안테나 체인(Chain)";
wl_adv.rxchainmask="RX 수신 안테나 체인(Chain)";



wl_adv.col1="CWmin";
wl_adv.col2="CWmax";
wl_adv.col3="AIFSN";
wl_adv.col4="TXOP(b)";
wl_adv.col5="TXOP(a/g)";

wl_adv.table3="WMM Tx retry limits, fallback limits and max rate parameters.";
wl_adv.txpcol1="S. Retry";
wl_adv.txpcol2="S. Fallbk";
wl_adv.txpcol3="L. Retry";
wl_adv.txpcol4="L. Fallbk";
wl_adv.txpcol5="Max Rate";
wl_adv.txprow1="AC BE TX 파라미터";
wl_adv.txprow2="AC BK TX 파라미터";
wl_adv.txprow3="AC VI TX 파라미터";
wl_adv.txprow4="AC VO TX 파라미터";

wl_adv.col6="Admin Forced";
wl_adv.row1="Background(낮게)";
wl_adv.row2="Best Effort(보통)";
wl_adv.row3="Video(우선)";
wl_adv.row4="Voice(최우선)";
wl_adv.table2="EDCA STA Parameters (클라이언트 -> AP)";
wl_adv.lng="Long";				//************* don't use .long ! *************	
wl_adv.shrt="Short";				//************* don't use .short ! **************		

//help container
hwl_adv.right2="자동 혹은 공유키(Shared Key)를 선택할 수 있습니다. 공유키 인증을 선택하신 경유 보안선택에서 WEP를 선택하셔야 합니다, 공유키를 선택하신 경우 네트워크상의 모든 장치에서 공유키 인증을 지원하여야 합니다.";

// ** Wireless_Basic.asp **//
wl_basic.titl="무선 기본";
wl_basic.h2="무선랜 물리 인터페이스 설정";
wl_basic.cardtype="Card Type";
wl_basic.legend="무선랜 기본설정";
wl_basic.label="무선랜 MAC 동작 모드";
wl_basic.label2="무선랜 PHY 동작 모드";
wl_basic.label3="무선네트워크 이름 (SSID)";
wl_basic.label4="무선 채널";
wl_basic.label5="무선 SSID 공개";
wl_basic.label6="통신거리의 유효범위 (ACK Timing)";
wl_basic.label7="802.11n 전송 모드";
wl_basic.scanlist="ScanList";
wl_basic.duallink="Dual Link";
wl_basic.parent="Parent IP";
wl_basic.masquerade="가상모드 / NAT";
wl_basic.ap="AP";
wl_basic.client="Client(클라이언트)";
wl_basic.repeater="Repeater";
wl_basic.repeaterbridge="Repeater Bridge";
wl_basic.clientBridge="Client Bridge";
wl_basic.clientRelayd="Client Bridge (Routed)";
wl_basic.adhoc="Adhoc";
wl_basic.wdssta="WDS Station";
wl_basic.wdsap="WDS AP";
wl_basic.mixed="Mixed";
wl_basic.greenfield="Greenfield";
wl_basic.preamble="Short Preamble";
wl_basic.clientRelaydDefaultGwMode="기본 GW 모드";
wl_basic.b="B-Only";
wl_basic.a="A-Only";
wl_basic.ac="AC-Only";
wl_basic.na="NA-Mixed";
wl_basic.ng="NG-Mixed";
wl_basic.n5="N-Only (5 GHz)";
wl_basic.n2="N-Only (2.4 GHz)";
wl_basic.g="G-Only";
wl_basic.bg="BG-Mixed";
wl_basic.n="N-Only";
wl_basic.rts="RTS Threshold";
wl_basic.rtsvalue="Threshold";
wl_basic.protmode="Protection Mode";
wl_basic.legend2="Radio Time Restrictions";
wl_basic.radio="Radio";
wl_basic.radiotimer="무선랜 타이머";
wl_basic.radio_on="무선랜 켜기";
wl_basic.radio_off="무선랜 끄기";
wl_basic.h2_v24="무선 물리 인터페이스 설정";
wl_basic.h2_vi="무선랜 가상 인터페이스 설정";
wl_basic.regdom="Regulatory Domain";
wl_basic.TXpower="송신 출력";
wl_basic.TXpowerFcc="최대 송신 출력 (FCC)";
wl_basic.AntGain="안테나 Gain";
wl_basic.diversity="다중송수신";
wl_basic.primary="메인";
wl_basic.secondary="보조";
wl_basic.vertical="Vertical";
wl_basic.horizontal="Horizontal";
wl_basic.adaptive="Adaptive";
wl_basic.internal="Internal";
wl_basic.external="External";
wl_basic.ghz24="2.4 GHz 출력";
wl_basic.ghz5="5 GHz 출력";
wl_basic.network="네트워크 설정";
wl_basic.unbridged="bridge하지 않기";
wl_basic.bridged="Bridge하기";
wl_basic.turbo="터보모드";
wl_basic.extrange="Extended Range(확장범위)";
wl_basic.supergcomp="Super G 데이터 압축";
wl_basic.supergff="Super G Fram 고속 전송";
wl_basic.extchannel="확장채널모드";
wl_basic.outband="Outdoor Band 아웃도어 밴드";
wl_basic.channel_width="채널 대역";
wl_basic.channel_wide="확장채널";
wl_basic.regulatory="SuperChannel";
wl_basic.chanshift="채널 이동";
wl_basic.specialmode="2.3 GHz 모드";
wl_basic.wifi_bonding="Wifi Bonding";
wl_basic.sifstime="OFDM SIFS Time";
wl_basic.preambletime="OFDM Preamble Time";
wl_basic.multicast="Multicast forwarding";
wl_basic.intmit="Noise Immunity";
wl_basic.noise_immunity="Noise Immunity Level(간섭 면제 레벨)";
wl_basic.ofdm_weak_det="OFDM Weak 검출";
wl_basic.radar="Radar 검출";
wl_basic.mtikie="MTik 호환성";
wl_basic.csma="Carrier Sense";
wl_basic.if_label="Label (옵션)";
wl_basic.if_info="Info (옵션)";
wl_basic.advanced_options="확장옵션";
wl_basic.rate_control="Rate Control 알고리즘";
wl_basic.ap83_vap_note="3개 이상의 가상 인터페이스를 추가하시면 추가된 가상 인터페이스의 일부 클라이언트에서 성능저하가 일어날수 있습니다.";

//help container
hwl_basic.right2="만일 G모드의 무선단말을 제외하고자 할 경우, <em>B-Only</em> 모드를 선택하여 주십시요. 만일 무선접속을 사용하지않으려는 경우, <em>Disable</em>을 선택하여 주십시요.<br/><b>주의 :</b> 무선방식(모드)을 변경하실경우, 일부 고급설정의 파라미터의 사용이 제한될 수 있습니다. (\"" + wl_adv.label16 + "\", \"" + wl_adv.label2 + "\" or \"" + wl_adv.label5 + "\").";
hwl_basic.right3="통신거리 유효범위: ";
hwl_basic.right4=" ACK 통신응답 타임아웃까지의 시간을 설정하실 수 있습니다. 0은 브로드컴 펌웨어의 ack timing을 비활성화 시킵니다. Atheros 기반의 펌웨어에서는, 0 은 자동 ACK timing 모드를 활성화 시킵니다.";
hwl_basic.right6="임의의 시간을 클릭하시면 무선 신호를 비활성화 혹은 활성화 합니다 (<em>green</em> 은 무선기능이 유효한 시간을, 그리고 <em>red</em>의 경우는 무효화되어 있는 시간대역을 지정하실 수 있습니다)";

// ** Fail_s.asp / Fail_u_s.asp / Fail.asp **//
fail.mess1="입력하신 값이 유효하지 않습니다, 다시한번 입력하여 주십시요.";
fail.mess2="업그레이트 실패.";


// ** Success*.asp / Reboot.asp  **//
success.saved="설정이 저장되었습니다.";
success.restore="설정이 복원되었습니다.<br/>장치가 재부팅을 합니다. 잠시만 기다려 주십시요...";
success.upgrade="성공적으로 업그레이드 되었습니다.<br/>장치가 재부팅을 합니다. 잠시만 기다려 주십시요...";
success.success_noreboot="설정이 완료되었습니다.";
success.success_reboot=success.success_noreboot + "<br />장치가 재부팅을 합니다. 잠시만 기다려 주십시요...";

success.alert_reset="모든 설정이 초기 값으로 복원되었습니다.<br /><br />";
success.alert1="다음 항목을 확인 하시고 한번 더 접속을 시작하여 주십시요:";
success.alert2="공유기의 IP 어드레스가 변경되었을 경우, 네트워크상의 사용자 어드레스를 반드시 재설정/갱신(release/renew)하여 하여야합니다 .";
success.alert3="무선랜(WLAN)을 통해서 접속이 되었을 경우, 네트워크에 들어가서셔 <em>계속</em>을 클릭하여 주십시요.";

// ** Logout.asp  **//
logout.message="성공적으로 로그아웃이 되었습니다.<br />DD-WRT 를 사용해 주셔서 감사합니다!";

// ** Setup Assistant **//
sas.title="설정도우미";
sas.internet_connection="인터넷 접속";
sas.network_settings="네트워크 설정";
sas.wireless_settings="무선 설정";
sas.other_settings="기타 설정";
sas.hwan="인터넷(WAN) 설정";

hsas.wan="WAN 인터페이스는 공유기를 인터넷 혹은 다른 네트워크에 접속시켜 줍니다. 이미 네트워크가 인터넷이 연결되어있는 환경에서 공유기를 억세스 포인트(AP)로써 사용할고자 할 경우  WAN 모드를 \무효(disabled)\"로 설정 변경하여 주십시요.";
hsas.h_routerip="Router IP(LAN측 접속)";
hsas.routerip="내부 네트워크에서 할당된 IP 어드레스 입니다. 만일 어드레스를 변경하신 경우, 변경을 적용하신 후 새 어드레스를 이용하여 Router Management(공유기 관리) 기능에 접속하셔야 합니다";
hsas.h_dhcp="DHCP";
hsas.dhcp="DHCP를 사용하시면 수동 설정이 필요없이 컴퓨터 또는 기타 네트워크 장치에 IP 주소를 자동으로 설정할 수 있습니다. DHCP 서버가 이미 로컬 네트워크상에 존재하는 경우 충돌을 피하기 위해 이 설정 무효화하여 주십시요.";
hsas.h_wireless_physical="Wireless Radio Interface(무선랜 인터페이스)";
hsas.wireless_physical="무선 LAN 인터페이스 설정에서 라우터의 무선 설정을 할 수 있습니다. 동작 모드(액세스 포인트, 클라이언트, 리피터)등으로 설정이 가능하며,  네트워크 이름 (SSID)을 변경, 채널 대역폭변경 등의 고급 설정을 하실 수 있습니다. 채널 대역폭을 표준 20MHz에서 변경하려면 무선 클라이언트가 변경하려는 채널 대역폭에 지원하여야하며  또한 올바른 설정을 할수 있는 기능을 지원하는지 확인하십시오.";
hsas.h_wireless_security="무선 보안";
hsas.wireless_security="보다 쉬운 클라이언트 장치 설정을 위하여 무선 네트워크 사용자 암호를 변경하실 수 있습니다. 무선 보안의 위해서 암호화 기능을 끄거나 WEP로 변경하는 것은 권하지 않습니다.";
hsas.h_routername="공유기 이름";
hsas.routername="공유기 이름은 네트워크에 있는 다른 장치들과의 통신과 보다 쉽게 장치를 인식하기 위하여 필요합니다.";
hsas.networking="네트워킹 설정 도움말";
hsas.wireless="무선 설정 도움말";
hsas.other="기타 설정 도움말";

// ** AOSS **//
haoss.basic="The \"AirStation One-Touch Secure System(버팔로 에어스테이션 원터치 보안 시스템)\" AOSS 기능을 사용하시면 자동으로 AOSS 대응 클라이언트가 억세스포인트에 접속할 수 있게 합니다.";
haoss.securitymodes="AOSS 보안 모드는 어떠한 타입의 보안모드를 AOSS에 사용할 것인지를 설정합니다. 만일 클라이언트에서 지원하는 보안방식이 유효하지 않을 경우 접속하실 수 없습니다.";
haoss.wps="라우터의 버튼 혹은 사용자 단말의 PIN을 사용하여 WPS에서 Wi-Fi 보호 설정을 지원합니다.";

ias.title="설정";
ias.card_info="설정 카드";
ias.edit_note="설정카드상의 임의의 정보를 클릭하셔서 편집하여 주십시요.";
ias.assistant="설정 도우미 실행";
ias.print_setup_card="설정 카드 인쇄";
ias.print_guest_card="방문자(Guest) 카드 인쇄";
ias.apply_changes="설정변경 적용";
ias.wlnetwork="무선 네트워크";
ias.wlinfo_2_4_GHz="(2.4 Ghz) - 802.11n/g/b와 호환됩니다";
ias.wlinfo_5_GHz="(5 Ghz) - 802.11n/a와 호환됩니다";
ias.hl_setup_card="설정 카드";
ias.hl_client_access="클라이언트 접속";
ias.hl_for_conf="환경 설정";
ias.hl_guest_card="방문자 카드";

// ************		OLD PAGES 		*******************************//		
// *********************** DHCPTable.asp *****************************//
dhcp.titl="DHCP Active IP Table";
dhcp.h2="DHCP Active IP Table";
dhcp.server="DHCP 서버 IP 어드레스 :";
dhcp.tclient="사용자 단말 호스트네임";

donate.mb="You may also donate through the Moneybookers account mb@dd-wrt.com";
